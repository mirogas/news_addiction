#mirogas

news-addiction
==============

Tables
------

Rate - is rate exchange table that represents currency number.
News - is table that represents one of day.
Category - is table of all categories from all days, has reference on 
new table(news_id). It means, that categories with equivalent news_id represent one day.


Prediction Tables
-----------------

Report is prediction for concrete currency
Prediction_Rates - key-value table day-currency_rate with reference to reports.

Installation
------------

###Install postgresql

1. sudo apt-get update
2. sudo apt-get install postgresql postgresql-contrib

###Check it

3. sudo -i -u postgres (it will connect to user postgres, that automatically created when do previous command)
4. psql (run psql)
5. \q (quit it)

###Easy connect
6. sudo -u postgres psql

###Create role(user)
From main user
7. sudo -u postgres createuser --interactive
    * name mirogas (It is very important)
    * y

###Create Database
8. sudo -u postgres createdb mirogas --password (password = mirogas)