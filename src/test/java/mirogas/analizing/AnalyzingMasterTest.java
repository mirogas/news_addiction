package mirogas.analizing;

import static java.time.LocalDate.now;
import static mirogas.application.service.MapperUtil.count;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.model.object.RateExchange;
import org.junit.Test;

/**
 * news_addiction Created on 09.06.17.
 */
public class AnalyzingMasterTest {

  private final AnalyzingMaster analyzingMaster = new AnalyzingMaster();

  @Test
  public void combine() throws Exception {
    final List<RateExchange> rateExchanges = Arrays.asList(
        RateExchange.builder().rate(10.0).date(now().minusDays(1)).build(),
        RateExchange.builder().rate(15.0).date(now()).build()
    );

    final List<News> newsList = Arrays.asList(
        News.builder().newsDate(now().minusDays(1)).categories(count(
            Category.builder().categoryId(1).categoryName("name1").newsId(1).build(),
            Category.builder().categoryId(2).categoryName("name2").newsId(1).build())
        ).build(),
        News.builder().newsDate(now()).categories(count(
            Category.builder().categoryId(2).categoryName("name2").newsId(1).build(),
            Category.builder().categoryId(3).categoryName("name3").newsId(1).build(),
            Category.builder().categoryId(4).categoryName("name4").newsId(1).build())
        ).build()
    );

    assertThat(analyzingMaster.combine(
        rateExchanges, newsList
    ))
        .hasSize(2)
        .matches(
            analyzingObjects -> analyzingObjects.get(0).getValue() == 10.0 &&
                analyzingObjects.get(1).getValue() == 15.0
        )
        .satisfies(
            analyzingObjects -> {
              assertThat(analyzingObjects.get(0))
                  .matches(
                      analyzingObject -> analyzingObject.getValue() == 10.0 &&
                          analyzingObject.getArguments().size() == 4
                  )
                  .satisfies(
                      analyzingObject -> assertThat(analyzingObject.getArguments())
                          .containsOnly(1.0, 0.0)

                  );

              assertThat(analyzingObjects.get(1))
                  .matches(
                      analyzingObject -> analyzingObject.getValue() == 15.0 &&
                          analyzingObject.getArguments().size() == 4
                  )
                  .satisfies(
                      analyzingObject -> assertThat(analyzingObject.getArguments())
                          .containsOnly(1.0, 0.0)

                  );
            }
        );

  }

  @Test
  public void fillArguments() throws Exception {
    final List<Category> allCategories = Arrays.asList(
        Category.builder().categoryId(1).categoryName("name1").newsId(1).build(),
        Category.builder().categoryId(2).categoryName("name2").newsId(1).build(),
        Category.builder().categoryId(3).categoryName("name3").newsId(1).build(),
        Category.builder().categoryId(4).categoryName("name4").newsId(1).build()
    );

    final Map<Category, Integer> categories = count(
        Category.builder().categoryId(1).categoryName("name1").newsId(1).build(),
        Category.builder().categoryId(1).categoryName("name1").newsId(1).build(),
        Category.builder().categoryId(4).categoryName("name4").newsId(1).build(),
        Category.builder().categoryId(4).categoryName("name4").newsId(1).build()
    );

    assertThat(AnalyzingMaster.fillArguments(
        allCategories, categories
    ))
        .hasSize(4)
        .containsExactly(2.0, 0.0, 0.0, 2.0);
  }

  @Test
  public void checkListsForEquivalentDatesTrue() throws Exception {
    final List<RateExchange> rateExchanges = Arrays.asList(
        RateExchange.builder().date(now()).build(),
        RateExchange.builder().date(now().minusDays(1)).build()
    );

    final List<News> newsList = Arrays.asList(
        News.builder().newsDate(now()).build(),
        News.builder().newsDate(now().minusDays(1)).build()
    );

    assertThat(AnalyzingMaster.checkListsForEquivalentDates(rateExchanges, newsList))
        .isTrue();
  }

  @Test
  public void checkListsForEquivalentDatesFalseSize() throws Exception {
    final List<RateExchange> rateExchanges = Collections.singletonList(
        RateExchange.builder().date(now()).build()
    );

    final List<News> newsList = Arrays.asList(
        News.builder().newsDate(now()).build(),
        News.builder().newsDate(now().minusDays(1)).build()
    );

    assertThat(AnalyzingMaster.checkListsForEquivalentDates(rateExchanges, newsList))
        .isFalse();
  }

  @Test
  public void checkListsForEquivalentDatesFalseDates() throws Exception {
    final List<RateExchange> rateExchanges = Arrays.asList(
        RateExchange.builder().date(now()).build(),
        RateExchange.builder().date(now().minusDays(1)).build()
    );

    final List<News> newsList = Arrays.asList(
        News.builder().newsDate(now()).build(),
        News.builder().newsDate(now().minusDays(2)).build()
    );

    assertThat(AnalyzingMaster.checkListsForEquivalentDates(rateExchanges, newsList))
        .isFalse();
  }

  @Test(expected = IllegalArgumentException.class)
  public void checkListsForEquivalentDatesThrowexceptionInCombine() {
    analyzingMaster.combine(
        Collections.emptyList(),
        Collections.singletonList(News.builder().build())
    );
  }

}