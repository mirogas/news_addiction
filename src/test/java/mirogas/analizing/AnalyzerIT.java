package mirogas.analizing;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;
import mirogas.application.Application;
import mirogas.application.TestConfig;
import mirogas.application.TestUtil;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 10.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class, TestConfig.class})
@ActiveProfiles(SPRING_PROFILE_TEST)
public class AnalyzerIT {

  @Autowired
  private List<Analyzer> analyzers;

  @Autowired
  private TestUtil testUtil;

  @Test
  @Transactional
  public void analyze() throws Exception {
    addInformationToTable();

    for (final Analyzer analyzer : analyzers) {
      analyzer.analyze(
          new SamplingInterval(LocalDate.now().minusDays(2), LocalDate.now()),
          Currency.RUB
      );

      assertThat(analyzer.predicate(
          new SamplingInterval(LocalDate.now().minusDays(2), LocalDate.now())
      ))
          .isGreaterThan(0);
    }
  }

  private void addInformationToTable() {
    final News news1 = testUtil.addNews(LocalDate.now());
    final News news2 = testUtil.addNews(LocalDate.now().minusDays(1));
    final News news3 = testUtil.addNews(LocalDate.now().minusDays(2));

    testUtil.addCategory(news1.getNewsId(), "category1", 1);
    testUtil.addCategory(news1.getNewsId(), "category2", 1);
    testUtil.addCategory(news1.getNewsId(), "category3", 1);

    testUtil.addCategory(news2.getNewsId(), "category2", 1);
    testUtil.addCategory(news2.getNewsId(), "category3", 1);
    testUtil.addCategory(news2.getNewsId(), "category4", 1);

    testUtil.addCategory(news3.getNewsId(), "category1", 1);
    testUtil.addCategory(news3.getNewsId(), "category4", 1);
    testUtil.addCategory(news3.getNewsId(), "category5", 1);

    testUtil.addRateExchange(45, Currency.RUB, LocalDate.now());
    testUtil.addRateExchange(50, Currency.RUB, LocalDate.now().minusDays(1));
    testUtil.addRateExchange(55, Currency.RUB, LocalDate.now().minusDays(2));
  }
}