package mirogas.analizing.result.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import org.junit.Test;

public class MinDiffErrorEstimatorTest {

  @Test
  public void estimateDoubleEqualSized() throws Exception {
    MinDiffErrorEstimator minDiffErrorEstimator =
        new MinDiffErrorEstimator();

    final double estimate = minDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }

  @Test
  public void estimateDoubleLittleLeft() throws Exception {
    MinDiffErrorEstimator minDiffErrorEstimator =
        new MinDiffErrorEstimator();

    final double estimate = minDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }

  @Test
  public void estimateDoubleRightLittle() throws Exception {
    MinDiffErrorEstimator minDiffErrorEstimator =
        new MinDiffErrorEstimator();

    final double estimate = minDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }
}