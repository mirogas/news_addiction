package mirogas.analizing.result.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import org.junit.Test;

public class QuadraticErrorEstimatorTest {

  @Test
  public void estimateDoubleEqualSized() throws Exception {
    QuadraticErrorEstimator quadraticErrorEstimator =
        new QuadraticErrorEstimator();

    final double estimate = quadraticErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 6, 0.01);
  }

  @Test
  public void estimateDoubleLittleLeft() throws Exception {
    QuadraticErrorEstimator quadraticErrorEstimator =
        new QuadraticErrorEstimator();

    final double estimate = quadraticErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 5, 0.01);
  }

  @Test
  public void estimateDoubleRightLittle() throws Exception {
    QuadraticErrorEstimator quadraticErrorEstimator =
        new QuadraticErrorEstimator();

    final double estimate = quadraticErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0)
    );

    assertEquals(estimate, 5, 0.01);
  }

}