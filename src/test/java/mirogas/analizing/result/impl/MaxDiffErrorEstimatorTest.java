package mirogas.analizing.result.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import org.junit.Test;

public class MaxDiffErrorEstimatorTest {

  @Test
  public void estimateDoubleEqualSized() throws Exception {
    MaxDiffErrorEstimator maxDiffErrorEstimator =
        new MaxDiffErrorEstimator();

    final double estimate = maxDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }

  @Test
  public void estimateDoubleLittleLeft() throws Exception {
    MaxDiffErrorEstimator maxDiffErrorEstimator =
        new MaxDiffErrorEstimator();

    final double estimate = maxDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }

  @Test
  public void estimateDoubleRightLittle() throws Exception {
    MaxDiffErrorEstimator maxDiffErrorEstimator =
        new MaxDiffErrorEstimator();

    final double estimate = maxDiffErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0)
    );

    assertEquals(estimate, 1.0, 0.01);
  }

}