package mirogas.analizing.result.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import org.junit.Test;

public class AbsErrorEstimatorTest {

  @Test
  public void estimateDoubleEqualSized() throws Exception {
    AbsErrorEstimator absErrorEstimator =
        new AbsErrorEstimator();

    final double estimate = absErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 6.0, 0.01);
  }

  @Test
  public void estimateDoubleLittleLeft() throws Exception {
    AbsErrorEstimator absErrorEstimator =
        new AbsErrorEstimator();

    final double estimate = absErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0, 7.0)
    );

    assertEquals(estimate, 5.0, 0.01);
  }

  @Test
  public void estimateDoubleRightLittle() throws Exception {
    AbsErrorEstimator absErrorEstimator =
        new AbsErrorEstimator();

    final double estimate = absErrorEstimator.estimate(
        Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0),
        Arrays.asList(2.0, 3.0, 4.0, 5.0, 6.0)
    );

    assertEquals(estimate, 5.0, 0.01);
  }
}