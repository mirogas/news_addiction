package mirogas.util;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.RateExchange;
import mirogas.application.model.object.Report;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class RandomUtil {

  private static AtomicInteger counter = new AtomicInteger(0);


  public static News randomNews() {
    return News.builder()
        .newsDate(randomDate())
        .categories(randomCategoryMap())
        .build();
  }

  public static Map<Category, Integer> randomCategoryMap() {
    return IntStream.range(0, RandomUtils.nextInt(5, 10))
        .mapToObj(i -> randomCategory())
        .collect(Collectors.toMap(Function.identity(), Category::getRepeats));
  }

  public static Category randomCategory() {
    return Category.builder()
        .categoryName(randomString())
        .repeats(RandomUtils.nextInt())
        .build();
  }

  public static RateExchange randomRateExchange() {
    return RateExchange.builder()
        .date(randomDate())
        .rate(randomRate())
        .currency(randomCurrency())
        .rateId(randomId())
        .build();
  }

  public static Currency randomCurrency() {
    switch (RandomUtils.nextInt(0, 4)) {
      case 0:
        return Currency.RUB;
      case 1:
        return Currency.EUR;
      case 2:
        return Currency.USD;
      case 3:
        return Currency.GBP;
    }
    throw new RuntimeException();
  }

  public static Report randomReport() {
    return Report.builder()
        .currency(randomCurrency())
        .name(randomString())
        .predicationRates(randomPredicationRates())
        .reportId(randomId())
        .build();
  }

  public static List<PredicationRate> randomPredicationRates() {
    return randomListBySupplier(RandomUtil::randomPredicationRate);
  }

  public static List<PredicationRate> randomPredicationRatesWithReportId(long reportId) {
    return randomListBySupplier(() -> randomPredicationRateWithReportId(reportId));
  }

  public static PredicationRate randomPredicationRateWithReportId(long reportId) {
    return PredicationRate.builder()
        .predicationRateId(randomId())
        .rate(randomRate())
        .rateDate(randomDate())
        .reportId(reportId)
        .build();

  }

  public static PredicationRate randomPredicationRate() {
    return PredicationRate.builder()
        .predicationRateId(randomId())
        .rate(randomRate())
        .rateDate(randomDate())
        .reportId(randomId())
        .build();
  }

  public static long randomId() {
    return RandomUtils.nextInt();
  }

  public static double randomRate() {
    return RandomUtils.nextDouble(20.0, 80.0);
  }

  public static String randomString() {
    return RandomStringUtils.random(50);
  }

  public static LocalDate randomDate() {
    return LocalDate.now().minusDays(counter.getAndIncrement());
  }

  public static <T> List<T> randomListBySupplier(Supplier<? extends T> supplier) {
    return Stream.generate(supplier)
        .limit(RandomUtils.nextInt(5, 15))
        .collect(Collectors.toList());
  }

  public static List<RateExchange> randomRateExchanges() {
    return randomListBySupplier(RandomUtil::randomRateExchange);
  }

  public static List<News> randomNewsList() {
    return randomListBySupplier(RandomUtil::randomNews);
  }
}
