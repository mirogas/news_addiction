package mirogas.application.model.interval.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import mirogas.application.model.interval.Interval;
import org.junit.Test;

/**
 * news_addiction Created on 09.06.17.
 */
public class LastMonthIntervalTest {

  private final Interval lastMonthInterval = new LastMonthInterval();

  @Test
  public void get() throws Exception {
    assertThat(lastMonthInterval.get())
        .matches(
            samplingInterval ->
                samplingInterval.getStart().getMonth().getValue() + 1 == LocalDate.now().getMonth()
                    .getValue() &&
                    samplingInterval.getEnd().getMonth().getValue() + 1 == LocalDate.now()
                        .getMonth().getValue()
        );
  }

}