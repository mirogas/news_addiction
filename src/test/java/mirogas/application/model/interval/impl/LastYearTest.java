package mirogas.application.model.interval.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.time.Month;
import mirogas.application.model.interval.Interval;
import org.junit.Test;

/**
 * news_addiction Created on 09.06.17.
 */
public class LastYearTest {

  private final Interval lastYear = new LastYearInterval();

  @Test
  public void get() throws Exception {
    assertThat(lastYear.get())
        .matches(
            samplingInterval -> samplingInterval.getStart().getMonth() == Month.JANUARY &&
                samplingInterval.getStart().getDayOfMonth() == 1 &&
                samplingInterval.getStart().getYear() + 1 == LocalDate.now().getYear() &&
                samplingInterval.getEnd().getMonth() == Month.DECEMBER &&
                samplingInterval.getEnd().getDayOfMonth() == 31
        );
  }

}