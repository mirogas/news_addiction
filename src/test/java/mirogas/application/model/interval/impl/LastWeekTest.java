package mirogas.application.model.interval.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.DayOfWeek;
import mirogas.application.model.interval.Interval;
import org.junit.Test;

/**
 * news_addiction Created on 09.06.17.
 */
public class LastWeekTest {

  private final Interval lastWeek = new LastWeekInterval();

  @Test
  public void get() throws Exception {
    assertThat(lastWeek.get())
        .matches(
            samplingInterval -> samplingInterval.getStart().getDayOfWeek() == DayOfWeek.MONDAY &&
                samplingInterval.getEnd().getDayOfWeek() == DayOfWeek.SUNDAY
        );
  }

}