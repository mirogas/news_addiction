package mirogas.application.dao.news;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 08.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class NewsDaoIT {

  @Autowired
  private NewsDao newsDao;

  @Test
  @Transactional
  @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
      "INSERT INTO news (news_date) VALUES (current_date)",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '1 day')",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '2 days')"
  })
  public void findAll() throws Exception {
    assertThat(newsDao.findAll())
        .hasSize(3);
  }

  @Test
  @Transactional
  public void findAllEmpty() {
    assertThat(newsDao.findAll())
        .hasSize(0);
  }

  @Test
  @Transactional
  public void add() throws Exception {
    assertThat(newsDao.add(LocalDate.now()))
        .hasFieldOrPropertyWithValue("newsDate", LocalDate.now());
  }

  @Test
  @Transactional
  public void addUniqueDate() throws Exception {
    newsDao.add(LocalDate.now());
    assertThatThrownBy(() -> newsDao.add(LocalDate.now()))
        .hasCauseExactlyInstanceOf(PSQLException.class);
  }

  @Test
  @Transactional
  @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, statements = {
      "INSERT INTO news (news_date) VALUES (current_date)",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '1 day')",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '2 days')",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '3 days')",
      "INSERT INTO news (news_date) VALUES (current_date - INTERVAL '4 days')"

  })
  public void findBetween() {
    assertThat(newsDao.findBetween(LocalDate.now().minusDays(3), LocalDate.now().minusDays(1)))
        .hasSize(3)
        .satisfies(
            newsList -> {
              assertThat(newsList.get(0))
                  .hasFieldOrPropertyWithValue("newsDate", LocalDate.now().minusDays(1));
              assertThat(newsList.get(1))
                  .hasFieldOrPropertyWithValue("newsDate", LocalDate.now().minusDays(2));
              assertThat(newsList.get(2))
                  .hasFieldOrPropertyWithValue("newsDate", LocalDate.now().minusDays(3));
            }
        );
  }
}