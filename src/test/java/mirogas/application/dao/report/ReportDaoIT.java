package mirogas.application.dao.report;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import mirogas.application.model.object.Currency;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 08.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportDaoIT {

  @Autowired
  private ReportDao reportDao;

  @Test
  @Transactional
  public void findAllEmpty() throws Exception {
    assertThat(reportDao.findAll())
        .hasSize(0);
  }

  @Test
  @Transactional
  public void findByNameAndCurrency() {
    reportDao.add("name", Currency.RUB.name());

    assertThat(reportDao.findByNameAndCurrency("name", Currency.RUB.name()))
        .hasValueSatisfying(
            report -> {
              assertThat(report.getCurrency()).isEqualTo(Currency.RUB);
              assertThat(report.getReportId()).isGreaterThan(0);
              assertThat(report.getName()).isEqualTo("name");
            }
        );
  }

  @Test
  @Transactional
  @Sql(statements = {
      "INSERT INTO reports(report_name, currency) VALUES('analyzer1', 'RUB')",
      "INSERT INTO reports(report_name, currency) VALUES('analyzer1', 'EUR')",
      "INSERT INTO reports(report_name, currency) VALUES('analyzer2', 'RUB')"
  })
  public void findAll() throws Exception {
    assertThat(reportDao.findAll())
        .hasSize(3);
  }

  @Test
  @Transactional
  public void findByNameAndCurrencyEmpty() {
    final String notExistingName = "notExistingName";
    assertThat(reportDao.findByNameAndCurrency(notExistingName, Currency.RUB.name()))
        .isEmpty();
  }

  @Test
  @Transactional
  public void add() throws Exception {
    assertThat(reportDao.add("reportType1", Currency.RUB.name()))
        .hasFieldOrPropertyWithValue("currency", Currency.RUB);
    assertThat(reportDao.add("reportType1", Currency.EUR.name()))
        .hasFieldOrPropertyWithValue("currency", Currency.EUR);
  }

  @Test
  @Transactional
  public void duplicateKey() {
    reportDao.add("name", Currency.RUB.name());

    assertThatThrownBy(() -> reportDao.add("name", Currency.RUB.name()))
        .hasCauseExactlyInstanceOf(PSQLException.class);
  }

}