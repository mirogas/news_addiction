package mirogas.application.dao.predication;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.util.RandomUtil.randomPredicationRatesWithReportId;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import mirogas.application.TestUtil;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.Report;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 08.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class PredicationRateDaoIT {

  @Autowired
  private TestUtil testUtil;

  @Autowired
  private PredicationRateDao predicationRateDao;

  @Test
  @Transactional
  public void findAll() {
    final Report report = testUtil.addReport("reportType1", Currency.RUB);

    final PredicationRate predicationRate1 = predicationRateDao
        .add(report.getReportId(), 15, LocalDate.now());
    final PredicationRate predicationRate2 = predicationRateDao
        .add(report.getReportId(), 15, LocalDate.now().minusDays(1));
    final PredicationRate predicationRate3 = predicationRateDao
        .add(report.getReportId(), 15, LocalDate.now().minusDays(2));

    assertThat(predicationRateDao.findAllByReportId(report.getReportId()))
        .containsOnlyOnce(
            predicationRate1, predicationRate2, predicationRate3
        );
  }

  @Test
  @Transactional
  public void findAllEmpty() {
    final Report report = testUtil.addReport("reportType1", Currency.EUR);
    assertThat(predicationRateDao.findAllByReportId(report.getReportId()))
        .hasSize(0);
  }

  @Test
  @Transactional
  public void findAllEmptyNotExistingReport() {
    final long notExistingId = 0;
    assertThat(predicationRateDao.findAllByReportId(notExistingId))
        .hasSize(0);
  }

  @Test
  @Transactional
  public void addAllEmpty() {
    assertThat(predicationRateDao.addAll(Collections.emptyList()))
        .isEqualTo(Collections.emptyList());
  }

  @Test
  @Transactional
  public void addAll() {
    final Report report = testUtil.addReport("name", Currency.EUR);

    final List<PredicationRate> predicationRates =
        randomPredicationRatesWithReportId(report.getReportId());

    final List<Double> rates = predicationRates.stream()
        .map(PredicationRate::getRate)
        .collect(Collectors.toList());

    assertThat(predicationRateDao.addAll(predicationRates))
        .hasSize(predicationRates.size());

    assertThat(predicationRateDao.findAllByReportId(report.getReportId()))
        .hasSize(predicationRates.size())
        .allSatisfy(
            el -> assertTrue(rates.stream()
                .anyMatch(v -> Math.abs(v - el.getRate()) < 0.001))
        );
  }
}