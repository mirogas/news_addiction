package mirogas.application.dao.rate;

import static java.time.LocalDate.now;
import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.application.model.object.Currency.RUB;
import static mirogas.util.RandomUtil.randomRateExchange;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 08.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class RateExchangeDaoIT {

  private static final AtomicInteger counter = new AtomicInteger(45);
  private static final AtomicInteger dayMinusCounter = new AtomicInteger(0);

  @Autowired
  private RateExchangeDao rateExchangeDao;

  private static RateExchange createRateExchange() {
    return RateExchange.builder()
        .rate(counter.incrementAndGet())
        .currency(RUB)
        .date(now().minusDays(dayMinusCounter.getAndIncrement()))
        .build();
  }

  @Test
  @Transactional
  public void findAll() throws Exception {
    final RateExchange rateExchange = createRateExchange();
    final RateExchange rateExchange1 = createRateExchange();
    final RateExchange rateExchange2 = createRateExchange();

    final long rateId = rateExchangeDao
        .add(rateExchange.getRate(), rateExchange.getCurrency().name(), rateExchange.getDate())
        .getRateId();
    final long rateId1 = rateExchangeDao
        .add(rateExchange1.getRate(), rateExchange1.getCurrency().name(), rateExchange1.getDate())
        .getRateId();
    final long rateId2 = rateExchangeDao
        .add(rateExchange2.getRate(), rateExchange2.getCurrency().name(), rateExchange2.getDate())
        .getRateId();

    rateExchange.setRateId(rateId);
    rateExchange1.setRateId(rateId1);
    rateExchange2.setRateId(rateId2);

    assertThat(rateExchangeDao.findAll())
        .containsOnlyOnce(
            rateExchange, rateExchange1, rateExchange2
        );
  }

  @Test
  @Transactional
  public void add() throws Exception {
    final RateExchange rateExchange = createRateExchange();

    assertThat(rateExchangeDao
        .add(rateExchange.getRate(), rateExchange.getCurrency().name(), rateExchange.getDate()))
        .hasFieldOrPropertyWithValue("rate", rateExchange.getRate())
        .hasFieldOrPropertyWithValue("currency", rateExchange.getCurrency())
        .hasFieldOrPropertyWithValue("date", rateExchange.getDate());
  }

  @Test
  @Transactional
  public void uniqueException() {
    rateExchangeDao.add(45, RUB.name(), now());

    assertThatThrownBy(() -> rateExchangeDao.add(45, RUB.name(), now()))
        .hasCauseExactlyInstanceOf(PSQLException.class);
  }

  @Test
  @Transactional
  @Sql(statements = {
      "INSERT INTO rates(rate,currency,rate_date) VALUES(45,'RUB',current_date)",
      "INSERT INTO rates(rate,currency,rate_date) VALUES(45,'RUB',current_date - INTERVAL '1 day')",
      "INSERT INTO rates(rate,currency,rate_date) VALUES(45,'RUB',current_date - INTERVAL '2 day')",
      "INSERT INTO rates(rate,currency,rate_date) VALUES(45,'EUR',current_date)",
  })
  public void findBetweenWithCurrency() {
    assertThat(rateExchangeDao.findBetweenWithCurrency(now().minusDays(1), now(), RUB.name()))
        .satisfies(
            rateExchanges -> {
              assertThat(rateExchanges.get(0))
                  .hasFieldOrPropertyWithValue("rate", 45.0)
                  .hasFieldOrPropertyWithValue("currency", Currency.RUB)
                  .hasFieldOrPropertyWithValue("date", LocalDate.now().minusDays(1));
              assertThat(rateExchanges.get(1))
                  .hasFieldOrPropertyWithValue("rate", 45.0)
                  .hasFieldOrPropertyWithValue("currency", Currency.RUB)
                  .hasFieldOrPropertyWithValue("date", LocalDate.now());
            }
        );
  }

  @Test
  @Transactional
  public void addAllEmpty() {
    assertThat(rateExchangeDao.addAll(Collections.emptyList()))
        .isEqualTo(Collections.emptyList());
  }

  @Test
  @Transactional
  public void addAll() {
    final List<RateExchange> rateExchanges = Arrays.asList(
        randomRateExchange(), randomRateExchange(), randomRateExchange()
    );

    final List<Double> rates = rateExchanges.stream()
        .map(RateExchange::getRate)
        .collect(Collectors.toList());

    assertThat(rateExchangeDao.addAll(rateExchanges))
        .hasSize(3);

    assertThat(rateExchangeDao.findAll())
        .hasSize(3)
        .allSatisfy(
            el -> assertTrue(rates.stream()
                .anyMatch(v -> Math.abs(v - el.getRate()) < 0.001))
        );
  }
}