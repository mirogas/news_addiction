package mirogas.application.dao.category;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import mirogas.application.TestUtil;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 08.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class CategoryDaoIT {

  private final static String CATEGORY_NAME = "CATEGORY_NAME";

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private TestUtil testUtil;

  private News news;

  public void setUp() {
    news = testUtil.addNews();
  }

  @Test
  @Transactional
  public void add() {
    setUp();
    assertThat(categoryDao.add(news.getNewsId(), CATEGORY_NAME, 2))
        .hasFieldOrPropertyWithValue("newsId", news.getNewsId())
        .hasFieldOrPropertyWithValue("categoryName", CATEGORY_NAME)
        .hasFieldOrPropertyWithValue("repeats", 2);
  }

  @Test
  @Transactional
  public void findAll() {
    setUp();
    final Category category1 = categoryDao.add(news.getNewsId(), "category1", 2);
    final Category category2 = categoryDao.add(news.getNewsId(), "category2", 1);
    final Category category3 = categoryDao.add(news.getNewsId(), "category3", 3);

    assertThat(categoryDao.findAllByNewsId(news.getNewsId()))
        .hasSize(3)
        .containsOnlyOnce(category1, category2, category3);
  }

  @Test
  @Transactional
  public void findAllEmpty() throws Exception {
    setUp();
    final long notExistingId = 0;
    assertThat(categoryDao.findAllByNewsId(notExistingId))
        .hasSize(0);
  }

  @Test
  @Transactional
  public void addAllEmpty() {
    setUp();
    assertThat(categoryDao.addAll(Collections.emptyList()))
        .isEqualTo(Collections.emptyList());
  }


  @Test
  @Transactional
  public void addAll() {
    setUp();
    final List<Category> categories = Arrays.asList(
        Category.builder().newsId(news.getNewsId())
            .categoryName("category1").repeats(3).build(),
        Category.builder().newsId(news.getNewsId())
            .categoryName("category2").repeats(4).build(),
        Category.builder().newsId(news.getNewsId())
            .categoryName("category3").repeats(5).build()
    );

    final List<String> categoryNames = categories.stream()
        .map(Category::getCategoryName)
        .collect(Collectors.toList());

    assertThat(categoryDao.addAll(categories))
        .hasSize(3);

    assertThat(categoryDao.findAllByNewsId(news.getNewsId()))
        .hasSize(3)
        .allSatisfy(
            el -> assertThat(categoryNames)
                .contains(el.getCategoryName())
        );
  }
}