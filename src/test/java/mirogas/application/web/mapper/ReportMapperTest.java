package mirogas.application.web.mapper;

import static mirogas.application.DateFormatConstant.DATE_TIME_FORMATTER;
import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.util.RandomUtil.randomPredicationRate;
import static mirogas.util.RandomUtil.randomRateExchange;
import static mirogas.util.RandomUtil.randomRateExchanges;
import static mirogas.util.RandomUtil.randomReport;
import static org.assertj.core.api.Assertions.assertThat;

import lombok.val;
import org.assertj.core.data.Percentage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportMapperTest {

  @Autowired
  private ReportMapper reportMapper;

  @Test
  public void mapRateExchanges() throws Exception {
    val rateExchanges = randomRateExchanges();

    assertThat(reportMapper.map(rateExchanges))
        .hasSize(rateExchanges.size());
  }

  @Test
  public void mapReport() throws Exception {
    val report = randomReport();

    assertThat(reportMapper.map(report))
        .satisfies(reportUI -> {
          assertThat(reportUI.getCurrency()).isEqualTo(report.getCurrency().name());
          assertThat(reportUI.getName()).isEqualTo(report.getName());
          assertThat(reportUI.getRates()).hasSize(report.getPredicationRates().size());
        });
  }

  @Test
  public void mapPredicationRate() throws Exception {
    val predicationRate = randomPredicationRate();

    assertThat(reportMapper.map(predicationRate))
        .satisfies(predicationRateUI -> {
          assertThat(predicationRateUI.getDate())
              .isEqualTo(predicationRate.getRateDate().format(DATE_TIME_FORMATTER));
          assertThat(predicationRateUI.getRate())
              .isCloseTo(predicationRate.getRate(), Percentage.withPercentage(95));
        });
  }

  @Test
  public void mapRateExchange() throws Exception {
    val rateExchange = randomRateExchange();

    assertThat(reportMapper.map(rateExchange))
        .satisfies(
            rateUI -> {
              assertThat(rateUI.getRate())
                  .isCloseTo(rateExchange.getRate(), Percentage.withPercentage(95));
              assertThat(rateUI.getDate())
                  .isEqualTo(rateExchange.getDate().format(DATE_TIME_FORMATTER));
            }
        );
  }

}