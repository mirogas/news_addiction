package mirogas.application.web.mapper;

import static mirogas.application.DateFormatConstant.DATE_TIME_FORMATTER;
import static mirogas.util.RandomUtil.randomNews;
import static mirogas.util.RandomUtil.randomNewsList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.util.stream.Collectors;
import lombok.val;
import mirogas.application.model.object.News;
import mirogas.application.web.model.NewsUI;
import org.junit.Test;

public class NewsMapperTest {

  private NewsMapper newsMapper = new NewsMapperImpl();

  @Test
  public void map() throws Exception {
    val news = randomNews();
    final NewsUI mappedNews = newsMapper.map(news);
    assertEqualsModelAndView(mappedNews, news);
  }

  private void assertEqualsModelAndView(NewsUI mappedNews, News news) {
    final Map<String, Integer> collect = news.getCategories().entrySet()
        .stream()
        .collect(Collectors.toMap(
            entry -> entry.getKey().getCategoryName(),
            Map.Entry::getValue
        ));
    assertThat(mappedNews)
        .satisfies(
            n -> assertThat(n.getDate()).isEqualTo(news.getNewsDate().format(DATE_TIME_FORMATTER)));

    assertThat(mappedNews.getCategories())
        .hasSize(collect.size())
        .allSatisfy(
            categoryUI -> assertThat(collect.get(categoryUI.getCategory()))
                .isEqualTo(categoryUI.getRepeats())
        );
  }

  @Test
  public void mapList() throws Exception {
    val newsList = randomNewsList();
    val mappedNewsList = newsMapper.map(newsList);

    assertThat(mappedNewsList)
        .hasSize(newsList.size());

    for (int i = 0; i < newsList.size(); i++) {
      assertEqualsModelAndView(mappedNewsList.get(i), newsList.get(i));
    }
  }

}