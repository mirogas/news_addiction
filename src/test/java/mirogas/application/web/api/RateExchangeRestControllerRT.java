package mirogas.application.web.api;

import static mirogas.util.RandomUtil.randomRateExchanges;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import lombok.val;
import mirogas.application.DateFormatConstant;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.rate.RateLoader;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 23.06.17.
 */

public class RateExchangeRestControllerRT extends BaseRestController {

  private final String start = "2017-07-01";
  private final String end = "2017-07-01";
  private final LocalDate parsedStart = LocalDate.parse(start);
  private final LocalDate parsedEnd = LocalDate.parse(end);
  private final SamplingInterval interval = new SamplingInterval(parsedStart, parsedEnd);
  @MockBean
  private RateLoader rateLoader;

  @Test
  @Transactional
  public void getByInterval() throws Exception {
    final List<RateExchange> rateExchanges = Arrays.asList(
        testUtil.addRateExchange(45, Currency.RUB, LocalDate.now().minusDays(2)),
        testUtil.addRateExchange(46, Currency.RUB, LocalDate.now().minusDays(1)),
        testUtil.addRateExchange(47, Currency.RUB, LocalDate.now())
    );

    mockMvc.perform(
        get("/api/rates/RUB?start=" + LocalDate.now().minusDays(2).toString() + "&end=" + LocalDate
            .now().toString())
            .accept(contentType)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].rate", is(rateExchanges.get(0).getRate())))
        .andExpect(jsonPath("$[0].date",
            is(rateExchanges.get(0).getDate().format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$[1].rate", is(rateExchanges.get(1).getRate())))
        .andExpect(jsonPath("$[1].date",
            is(rateExchanges.get(1).getDate().format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$[2].rate", is(rateExchanges.get(2).getRate())))
        .andExpect(jsonPath("$[2].date",
            is(rateExchanges.get(2).getDate().format(DateFormatConstant.DATE_TIME_FORMATTER))));

  }

  @Test
  @Transactional
  public void load() throws Exception {
    val rateExchanges = randomRateExchanges();
    when(rateLoader.loadByInterval(interval, Currency.RUB))
        .thenReturn(rateExchanges);

    mockMvc.perform(post("/api/rates/RUB/load?start=" + start + "&end=" + end)
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(jsonPath("$[0].rate", closeTo(rateExchanges.get(0).getRate(), 0.001)))
        .andExpect(jsonPath("$[0].date", is(rateExchanges.get(0).getDate()
            .format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$[1].rate", closeTo(rateExchanges.get(1).getRate(), 0.001)))
        .andExpect(jsonPath("$[1].date", is(rateExchanges.get(1).getDate()
            .format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$[2].rate", closeTo(rateExchanges.get(2).getRate(), 0.001)))
        .andExpect(jsonPath("$[2].date", is(rateExchanges.get(2).getDate()
            .format(DateFormatConstant.DATE_TIME_FORMATTER))));
  }

}