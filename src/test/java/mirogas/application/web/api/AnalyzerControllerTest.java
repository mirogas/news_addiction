package mirogas.application.web.api;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Test;
import org.springframework.http.MediaType;

public class AnalyzerControllerTest extends BaseRestController {

  @Test
  public void getAllAnalyzerNames() throws Exception {
    mockMvc.perform(get("/api/analyzers/all")
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0]", is("BackwardPolynomialRegressionAnalyzer")))
        .andExpect(jsonPath("$[1]", is("EasyPolynomialRegressionAnalyzer")))
        .andExpect(jsonPath("$[2]", is("EasyRegressionAnalyzer")))
        .andExpect(jsonPath("$[3]", is("ExponentialPolynomialRegressionAnalyzer")))
        .andExpect(jsonPath("$[4]", is("ToOneArgumentRegressionAnalyzer")));
  }
}