package mirogas.application.web.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import mirogas.analizing.Analyzer;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

public class AnalyzerControllerMockTest {

  private final String start = "2017-07-01";
  private final String end = "2017-07-01";
  private final LocalDate parsedStart = LocalDate.parse(start);
  private final LocalDate parsedEnd = LocalDate.parse(end);
  @Mock
  private Analyzer backwardPolynomialRegressionAnalyzer;
  @Mock
  private Analyzer easyPolynomialRegressionAnalyzer;
  @Mock
  private Analyzer easyRegressionAnalyzer;
  @Mock
  private Analyzer exponentialPolynomialRegressionAnalyzer;
  @Mock
  private Analyzer toOneArgumentRegressionAnalyzer;
  private AnalyzerController analyzerController;
  private List<Analyzer> analyzerList;

  private static void assertThatAllOfAnalyzersSatisfyOneInteraction(
      List<Analyzer> analyzerList, LocalDate parsedStart, LocalDate parsedEnd
  ) {
    assertThat(analyzerList)
        .allSatisfy(analyzer -> verify(analyzer, times(1))
            .analyze(eq(new SamplingInterval(parsedStart, parsedEnd)), eq(Currency.EUR)));
  }

  @Before
  public void setUp() {
    initMocks(this);
    analyzerList = Arrays.asList(
        backwardPolynomialRegressionAnalyzer,
        easyPolynomialRegressionAnalyzer,
        easyRegressionAnalyzer,
        exponentialPolynomialRegressionAnalyzer,
        toOneArgumentRegressionAnalyzer
    );
    analyzerController = new AnalyzerController(
        analyzerList
    );
  }

  @Test
  public void analyze() throws Exception {
    startAnalyzeAndCheckThatStatusNoContent();

    assertThatAllOfAnalyzersSatisfyOneInteraction(analyzerList, parsedStart, parsedEnd);
  }

  @Test
  public void analyzeAfterException() {
    doThrow(new RuntimeException()).when(backwardPolynomialRegressionAnalyzer)
        .analyze(any(SamplingInterval.class), any(Currency.class));

    startAnalyzeAndCheckThatStatusNoContent();
    assertThatAllOfAnalyzersSatisfyOneInteraction(analyzerList, parsedStart, parsedEnd);
  }

  @Test
  public void analyzeAfterExceptionsFromAllAnalyzers() {
    for (Analyzer analyzer : analyzerList) {
      doThrow(new RuntimeException()).when(analyzer)
          .analyze(any(SamplingInterval.class), any(Currency.class));
    }

    startAnalyzeAndCheckThatStatusNoContent();
    assertThatAllOfAnalyzersSatisfyOneInteraction(analyzerList, parsedStart, parsedEnd);
  }

  @Test
  public void predicate() {
    startPredicateAndCheckThatStatusNoContent();

    assertThatAllOfAnalyzersSatisfyOneInteractionWithPredicate(analyzerList, parsedStart,
        parsedEnd);
  }

  private void assertThatAllOfAnalyzersSatisfyOneInteractionWithPredicate(
      List<Analyzer> analyzerList, LocalDate parsedStart, LocalDate parsedEnd
  ) {
    assertThat(analyzerList)
        .allSatisfy(analyzer -> verify(analyzer, times(1))
            .predicate(eq(new SamplingInterval(parsedStart, parsedEnd))));
  }

  private void startPredicateAndCheckThatStatusNoContent() {
    assertThat(analyzerController.predicate(start, end))
        .satisfies(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT));
  }

  private void startAnalyzeAndCheckThatStatusNoContent() {
    assertThat(analyzerController.analyze("EUR", start, end))
        .satisfies(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT));
  }
}
