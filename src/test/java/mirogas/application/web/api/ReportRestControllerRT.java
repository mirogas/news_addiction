package mirogas.application.web.api;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import lombok.val;
import mirogas.application.DateFormatConstant;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.Report;
import mirogas.application.service.report.ReportService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 23.06.17.
 */

public class ReportRestControllerRT extends BaseRestController {

  @Autowired
  private ReportService reportService;

  @Test
  @Transactional
  public void getReport() throws Exception {
    final Report report = testUtil.addReport("reportName", Currency.RUB);
    final List<PredicationRate> predicationRates =
        Arrays
            .asList(testUtil.addPredicationRate(report.getReportId(), LocalDate.now().minusDays(2)),
                testUtil.addPredicationRate(report.getReportId(), LocalDate.now().minusDays(1)),
                testUtil.addPredicationRate(report.getReportId(), LocalDate.now())
            );

    mockMvc.perform(get("/api/report/name/reportName/currency/RUB")
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.name", is("reportName")))
        .andExpect(jsonPath("$.currency", is(Currency.RUB.name())))
        .andExpect(jsonPath("$.rates[0].rate", is(predicationRates.get(0).getRate())))
        .andExpect(jsonPath("$.rates[0].date",
            is(predicationRates.get(0).getRateDate()
                .format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$.rates[1].rate", is(predicationRates.get(1).getRate())))
        .andExpect(jsonPath("$.rates[1].date",
            is(predicationRates.get(1).getRateDate()
                .format(DateFormatConstant.DATE_TIME_FORMATTER))))
        .andExpect(jsonPath("$.rates[2].rate", is(predicationRates.get(2).getRate())))
        .andExpect(jsonPath("$.rates[2].date",
            is(predicationRates.get(2).getRateDate()
                .format(DateFormatConstant.DATE_TIME_FORMATTER))));
  }

  @Test
  @Transactional
  public void getAllPossibleReports() throws Exception {
    val objectMapper = new ObjectMapper();

    mockMvc.perform(get("/api/report//possible")
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().json(objectMapper.writeValueAsString(reportService.findAllNames())));

  }

}