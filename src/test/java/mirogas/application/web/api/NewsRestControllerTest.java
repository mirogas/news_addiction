package mirogas.application.web.api;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Collections;
import lombok.val;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.service.news.NewsLoader;
import mirogas.application.service.news.NewsService;
import mirogas.application.web.mapper.NewsMapper;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

public class NewsRestControllerTest extends BaseRestController {

  private final String start = "2017-07-01";
  private final String end = "2017-07-01";
  private final LocalDate parsedStart = LocalDate.parse(start);
  private final LocalDate parsedEnd = LocalDate.parse(end);
  @MockBean
  private NewsLoader newsLoader;
  @MockBean
  private NewsMapper newsMapper;
  @MockBean
  private NewsService newsService;

  @Test
  public void load() throws Exception {
    val interval = new SamplingInterval(parsedStart, parsedEnd);
    when(newsLoader.loadByInterval(interval))
        .thenReturn(Collections.emptyList());
    when(newsService.addAll(Collections.emptyList()))
        .thenReturn(Collections.emptyList());
    when(newsMapper.map(Collections.emptyList()))
        .thenReturn(Collections.emptyList());

    mockMvc.perform(post("/api/news/load?start=" + start + "&end=" + end)
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().json("[]"));

    verify(newsLoader, times(1))
        .loadByInterval(interval);
    verify(newsService, times(1))
        .addAll(Collections.emptyList());
    verify(newsMapper, times(1))
        .map(Collections.emptyList());
  }

  @Test
  public void show() throws Exception {
    when(newsService.findNewsByInterval(any(SamplingInterval.class)))
        .thenReturn(Collections.emptyList());
    when(newsMapper.map(Collections.emptyList()))
        .thenReturn(Collections.emptyList());

    mockMvc.perform(get("/api/news?start=" + start + "&end=" + end)
        .accept(contentType)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(200))
        .andExpect(content().json("[]"));

    verify(newsService, times(1))
        .findNewsByInterval(eq(new SamplingInterval(parsedStart, parsedEnd)));
    verify(newsMapper, times(1))
        .map(Collections.emptyList());
  }


}