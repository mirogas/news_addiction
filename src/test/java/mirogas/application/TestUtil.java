package mirogas.application;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;

import java.time.LocalDate;
import java.util.Random;
import lombok.AllArgsConstructor;
import mirogas.application.dao.category.CategoryDao;
import mirogas.application.dao.news.NewsDao;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.dao.rate.RateExchangeDao;
import mirogas.application.dao.report.ReportDao;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.RateExchange;
import mirogas.application.model.object.Report;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 08.06.17.
 */

@Service
@Profile(value = SPRING_PROFILE_TEST)
@AllArgsConstructor
public class TestUtil {

  private final RateExchangeDao rateExchangeDao;
  private final NewsDao newsDao;
  private final ReportDao reportDao;
  private final PredicationRateDao predicationRateDao;
  private final CategoryDao categoryDao;

  private final Random random = new Random();


  public RateExchange addRateExchange() {
    return rateExchangeDao.add(random.nextInt(100), Currency.RUB.name(), LocalDate.now());
  }

  public RateExchange addRateExchange(final double rate, final Currency currency,
      final LocalDate date) {
    return rateExchangeDao.add(rate, currency.name(), date);
  }

  public News addNews() {
    return newsDao.add(LocalDate.now());
  }

  public News addNews(final LocalDate date) {
    return newsDao.add(date);
  }

  public Report addReport(final String name, final Currency currency) {
    return reportDao.add(name, currency.name());
  }

  public PredicationRate addPredicationRate(final long reportId, final LocalDate date) {
    return predicationRateDao.add(
        reportId,
        random.nextInt(100),
        date
    );
  }

  public Category addCategory(final long newsId, final String categoryName, final int repeats) {
    return categoryDao.add(newsId, categoryName, repeats);
  }
}
