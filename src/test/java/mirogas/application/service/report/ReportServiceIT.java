package mirogas.application.service.report;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.util.RandomUtil.randomCurrency;
import static mirogas.util.RandomUtil.randomString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import mirogas.application.TestUtil;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.Report;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 09.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class ReportServiceIT {

  @Autowired
  private ReportService reportService;

  @Autowired
  private TestUtil testUtil;

  @Test
  @Transactional
  public void findByNameAndCurrency() throws Exception {
    final Report report = testUtil.addReport("name", Currency.RUB);
    final PredicationRate predicationRate = testUtil
        .addPredicationRate(report.getReportId(), LocalDate.now());
    final PredicationRate predicationRate1 = testUtil
        .addPredicationRate(report.getReportId(), LocalDate.now().minusDays(1));
    final PredicationRate predicationRate2 = testUtil
        .addPredicationRate(report.getReportId(), LocalDate.now().minusDays(2));

    report.setPredicationRates(Arrays.asList(predicationRate, predicationRate1, predicationRate2));

    assertThat(reportService.findByNameAndCurrency("name", Currency.RUB))
        .isEqualToComparingFieldByFieldRecursively(report);
  }

  @Test
  @Transactional
  public void findByNameAndCurrencyNotFind() {
    assertThatThrownBy(() -> reportService.findByNameAndCurrency("some", Currency.RUB))
        .hasMessage("Can't find report");
  }

  @Test
  @Transactional
  public void add() {
    assertThat(reportService.add(ReportServiceIT.class, Currency.RUB))
        .hasFieldOrPropertyWithValue("name", "ReportServiceIT")
        .hasFieldOrPropertyWithValue("currency", Currency.RUB);
  }

  @Test
  @Transactional
  public void findAll() {
    final List<String> collect = IntStream.range(0, RandomUtils.nextInt(5, 100))
        .mapToObj(i -> randomString())
        .peek(name -> testUtil.addReport(name, randomCurrency()))
        .collect(Collectors.toList());

    assertThat(reportService.findAllNames())
        .isEqualTo(collect);
  }

}