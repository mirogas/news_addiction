package mirogas.application.service.category;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import lombok.val;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.service.news.NewsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class CategoryServiceIT {

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private NewsService newsService;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Test
  @Transactional
  public void findAllByNewsId() throws Exception {
    val news = addNewsToDb();

    final long newsId = news.getNewsId();

    categoryService.add(newsId, "categoryName1", 1);
    categoryService.add(newsId, "categoryName2", 2);
    categoryService.add(newsId, "categoryName3", 3);

    final Map<Category, Integer> allByNewsId = categoryService.findAllByNewsId(newsId);
    assertThat(allByNewsId)
        .hasSize(3);

    assertThat(allByNewsId.keySet().stream().map(Category::getCategoryName))
        .contains("categoryName1", "categoryName2", "categoryName3");

    assertThat(allByNewsId.values())
        .contains(1, 2, 3);
  }

  @Test
  @Transactional
  public void add() throws Exception {
    val news = addNewsToDb();

    val category = categoryService.add(news.getNewsId(), "someCategory", 2);

    val categoryName = jdbcTemplate.queryForObject(
        "SELECT category_name FROM categories WHERE category_id = ?",
        new Object[]{category.getCategoryId()},
        String.class
    );

    assertThat(categoryName).isEqualTo("someCategory");
  }

  private News addNewsToDb() {
    return newsService.addAll(Collections.singletonList(
        News.builder().newsDate(LocalDate.now()).build()
    )).get(0);
  }

}