package mirogas.application.service;

import static java.time.temporal.ChronoUnit.DAYS;
import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.util.RandomUtil.randomCurrency;
import static mirogas.util.RandomUtil.randomRate;
import static mirogas.util.RandomUtil.randomString;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import lombok.val;
import mirogas.analizing.result.ErrorEstimator;
import mirogas.application.TestUtil;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.RateExchange;
import mirogas.application.model.object.Report;
import mirogas.application.web.model.TableResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class TableServiceTest {

  private final String start = "2017-01-01";
  private final String end = "2017-07-01";
  private final LocalDate parsedStart = LocalDate.parse(start);
  private final LocalDate parsedEnd = LocalDate.parse(end);
  private final long between = DAYS.between(parsedStart, parsedEnd);
  private final Currency currency = randomCurrency();
  @Autowired
  private List<ErrorEstimator> errorEstimators;
  @Autowired
  private TableService tableService;
  @Autowired
  private TestUtil testUtil;
  private List<RateExchange> realRateExchanges;

  public void setUp() {
    realRateExchanges = Stream
        .iterate(parsedStart, current -> current.plusDays(1))
        .limit(between)
        .map(date -> testUtil.addRateExchange(randomRate(), currency, date))
        .collect(Collectors.toList());
  }

  @Test
  @Transactional
  public void estimate() throws Exception {
    setUp();
    val reports = IntStream.range(0, 5)
        .mapToObj(i -> testUtil.addReport(randomString(), currency))
        .collect(Collectors.toList());

    final List<List<PredicationRate>> collect = reports.stream()
        .map(this::addRatesToReport)
        .collect(Collectors.toList());

    final TableResult tableResult = tableService.estimate(reports, currency);
    assertThat(tableResult.getVerticalNames())
        .hasSize(errorEstimators.size() + 1);

    assertThat(tableResult.getHorizontalNames())
        .hasSize(reports.size());

    assertThat(realRateExchanges)
        .isNotNull();
  }

  private List<PredicationRate> addRatesToReport(Report report) {
    val predicationRates = Stream.iterate(parsedStart, current -> current.plusDays(1))
        .limit(between)
        .map(date -> testUtil.addPredicationRate(report.getReportId(), date))
        .collect(Collectors.toList());
    report.setPredicationRates(predicationRates);
    return predicationRates;
  }

}