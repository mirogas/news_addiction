package mirogas.application.service.rate;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import mirogas.application.TestUtil;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 10.06.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class RateExchangeServiceIT {

  @Autowired
  private RateExchangeService rateExchangeService;

  @Autowired
  private TestUtil testUtil;

  @Test
  @Transactional
  public void findBetweenWithCurrency() throws Exception {
    final RateExchange rateExchange1 = testUtil
        .addRateExchange(45.0, Currency.RUB, LocalDate.now());
    final RateExchange rateExchange2 = testUtil
        .addRateExchange(47.0, Currency.RUB, LocalDate.now().minusDays(1));
    testUtil.addRateExchange(48.0, Currency.EUR, LocalDate.now());

    assertThat(rateExchangeService.findBetweenWithCurrency(
        new SamplingInterval(LocalDate.now().minusDays(1), LocalDate.now()), Currency.RUB)
    )
        .containsOnlyOnce(rateExchange1, rateExchange2);

  }

  @Test
  @Transactional
  public void add() {
    assertThat(rateExchangeService.add(45, Currency.RUB, LocalDate.now()))
        .hasFieldOrPropertyWithValue("rate", 45.0)
        .hasFieldOrPropertyWithValue("currency", Currency.RUB)
        .hasFieldOrPropertyWithValue("date", LocalDate.now());
  }
}