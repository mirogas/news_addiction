package mirogas.application.service.rate;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 09.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class RateLoaderIT {

  @Autowired
  private RateLoader rateLoader;

  @Test
  @Transactional
  public void loadByInterval() throws Exception {
    final LocalDate startDate = LocalDate.of(2017, 6, 20);
    final LocalDate endDate = LocalDate.of(2017, 6, 21);

    assertThat(rateLoader.loadByInterval(
        new SamplingInterval(startDate, endDate), Currency.EUR
    )).hasSize(2)
        .allMatch(rateExchange -> rateExchange.getCurrency() == Currency.EUR)
        .satisfies(
            list -> {
              assertThat(list.get(0))
                  .hasFieldOrPropertyWithValue("date", startDate)
                  .hasFieldOrPropertyWithValue("rate", 57.9585);
              assertThat(list.get(1))
                  .hasFieldOrPropertyWithValue("date", endDate)
                  .hasFieldOrPropertyWithValue("rate", 58.5786);
            }
        );

  }

}