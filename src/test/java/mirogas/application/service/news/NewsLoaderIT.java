package mirogas.application.service.news;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 09.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class NewsLoaderIT {

  @Autowired
  private NewsLoader newsLoader;

  private static Category fromString(final String string) {
    return Category.builder().categoryName(string).build();
  }

  @Test
  @Transactional
  public void loadByInterval() throws Exception {
    final LocalDate startDate = LocalDate.of(2017, 6, 20);
    final LocalDate endDate = LocalDate.of(2017, 6, 21);

    assertThat(newsLoader.loadByInterval(new SamplingInterval(startDate, endDate)))
        .hasSize(2)
        .satisfies(
            list -> {
              assertThat(list.get(0).getCategories()).hasSize(24)
                  .containsKeys(fromString("Спорт"), fromString("Происшествия"),
                      fromString("Город"));
              assertThat(list.get(1).getCategories()).hasSize(28)
                  .containsKeys(fromString("Общество"),
                      fromString("47новостей из Ленинградской Области"), fromString("Общество"));
            }
        );
  }

}