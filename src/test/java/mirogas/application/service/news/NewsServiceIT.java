package mirogas.application.service.news;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_TEST;
import static mirogas.util.RandomUtil.randomNews;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import lombok.val;
import mirogas.application.TestUtil;
import mirogas.application.dao.category.CategoryDao;
import mirogas.application.dao.news.NewsDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * news_addiction Created on 10.06.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(SPRING_PROFILE_TEST)
public class NewsServiceIT {

  @Autowired
  private NewsService newsService;

  @Autowired
  private TestUtil testUtil;

  @Autowired
  private NewsDao newsDao;

  @Autowired
  private CategoryDao categoryDao;

  @Test
  @Transactional
  public void findNewsByInterval() throws Exception {
    final News news1 = testUtil.addNews(LocalDate.now());
    final News news2 = testUtil.addNews(LocalDate.now().minusDays(1));

    final Map<Category, Integer> categories1 = new HashMap<>();
    categories1.put(testUtil.addCategory(news1.getNewsId(), "category1", 2), 2);
    categories1.put(testUtil.addCategory(news1.getNewsId(), "category2", 1), 1);
    categories1.put(testUtil.addCategory(news1.getNewsId(), "category3", 1), 1);

    final Map<Category, Integer> categories2 = new HashMap<>();
    categories2.put(testUtil.addCategory(news2.getNewsId(), "category3", 1), 1);
    categories2.put(testUtil.addCategory(news2.getNewsId(), "category4", 1), 1);
    categories2.put(testUtil.addCategory(news2.getNewsId(), "category2", 2), 2);

    news1.setCategories(categories1);
    news2.setCategories(categories2);

    assertThat(newsService.findNewsByInterval(
        new SamplingInterval(LocalDate.now().minusDays(1), LocalDate.now())
    ))
        .containsOnlyOnce(news1, news2);
  }

  @Test
  @Transactional
  public void addAllTest() {
    val newsList = newsService.addAll(Arrays.asList(randomNews(), randomNews(), randomNews()));

    assertThat(newsDao.findAll())
        .hasSize(3)
        .allSatisfy(news -> {
          assertThat(newsList.stream().map(News::getNewsDate)).contains(news.getNewsDate());
          assertThat(newsList.stream().map(News::getNewsId)).contains(news.getNewsId());
        });

    for (News news : newsList) {
      assertThat(categoryDao.findAllByNewsId(news.getNewsId()))
          .allSatisfy(
              category -> {
                assertThat(
                    news.getCategories().keySet().stream().map(Category::getCategoryName)
                ).contains(category.getCategoryName());
                assertThat(
                    news.getCategories().keySet().stream().map(Category::getRepeats)
                ).contains(category.getRepeats());
              }
          );
    }
  }
}