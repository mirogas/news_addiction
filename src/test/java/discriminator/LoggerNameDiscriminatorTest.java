package discriminator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.Test;

public class LoggerNameDiscriminatorTest {

  private LoggerNameDiscriminator loggerNameDiscriminator = new LoggerNameDiscriminator();

  @Test
  public void getDiscriminatingValue() {
    ILoggingEvent iLoggingEvent = mock(ILoggingEvent.class);
    when(iLoggingEvent.getLoggerName()).thenReturn("name");

    assertThat(loggerNameDiscriminator.getDiscriminatingValue(iLoggingEvent))
        .isEqualTo("name");
  }

}