package mirogas.application.web.mapper;

import java.util.List;
import mirogas.application.DateFormatConstant;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.RateExchange;
import mirogas.application.model.object.Report;
import mirogas.application.web.model.RateUI;
import mirogas.application.web.model.ReportUI;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * news_addiction Created on 09.06.17.
 */

@Mapper(componentModel = "spring")
public interface ReportMapper {

  List<RateUI> map(List<RateExchange> rateExchanges);

  @Mapping(target = "rates", source = "predicationRates")
  ReportUI map(Report report);

  @Mapping(target = "date", source = "rateDate", dateFormat = DateFormatConstant.DATE_FORMAT)
  RateUI map(PredicationRate predicationRate);

  @Mapping(target = "date", source = "date", dateFormat = DateFormatConstant.DATE_FORMAT)
  RateUI map(RateExchange rate);
}
