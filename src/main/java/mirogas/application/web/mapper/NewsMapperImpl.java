package mirogas.application.web.mapper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import mirogas.application.DateFormatConstant;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.web.model.CategoryUI;
import mirogas.application.web.model.NewsUI;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 23.06.17.
 */

@Service
public class NewsMapperImpl implements NewsMapper {

  public static List<CategoryUI> map(final Map<Category, Integer> map) {
    return map.entrySet().stream()
        .map(
            categoryIntegerEntry ->
                CategoryUI.builder()
                    .category(categoryIntegerEntry.getKey().getCategoryName())
                    .repeats(categoryIntegerEntry.getValue()).build()
        )
        .collect(Collectors.toList());
  }

  @Override
  public List<NewsUI> map(final List<News> newsList) {
    return newsList.stream()
        .map(this::map)
        .collect(Collectors.toList());
  }

  @Override
  public NewsUI map(final News news) {
    return NewsUI.builder()
        .date(news.getNewsDate().format(DateFormatConstant.DATE_TIME_FORMATTER))
        .categories(map(news.getCategories()))
        .build();
  }
}
