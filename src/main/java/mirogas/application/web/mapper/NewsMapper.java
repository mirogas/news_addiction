package mirogas.application.web.mapper;

import java.util.List;
import mirogas.application.model.object.News;
import mirogas.application.web.model.NewsUI;

/**
 * news_addiction Created on 23.06.17.
 */
public interface NewsMapper {

  List<NewsUI> map(List<News> newsList);

  NewsUI map(News news);
}
