package mirogas.application.web.api;

import java.time.LocalDate;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.service.rate.RateLoader;
import mirogas.application.web.mapper.ReportMapper;
import mirogas.application.web.model.RateUI;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * news_addiction Created on 23.06.17.
 */

@RestController
@RequestMapping(value = "api/rates")
@RequiredArgsConstructor
public class RateExchangeRestController {

  private final RateExchangeService rateExchangeService;
  private final ReportMapper reportMapper;
  private final RateLoader rateLoader;

  @PostMapping(value = "{currency}/load")
  public List<RateUI> load(
      @PathVariable("currency") @NonNull final String currency,
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    final List<RateExchange> rateExchanges = rateLoader.loadByInterval(
        new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end)),
        Currency.valueOf(currency)
    );
    return reportMapper.map(rateExchangeService.addAll(rateExchanges));
  }

  @GetMapping(value = "{currency}")
  public List<RateUI> getByInterval(
      @PathVariable("currency") @NonNull final String currency,
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    return reportMapper.map(rateExchangeService.findBetweenWithCurrency(
        new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end)),
        Currency.valueOf(currency)
    ));
  }
}
