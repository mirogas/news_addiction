package mirogas.application.web.api;

import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_RANDOM_DATA;

import lombok.val;
import mirogas.application.service.impl.RateLoaderMock;
import mirogas.application.service.policy.MapPolicy;
import mirogas.application.service.policy.ReducePolicy;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/switcher")
@Profile(SPRING_PROFILE_RANDOM_DATA)
public class RandomDataSwitcher {

  private RateLoaderMock rateLoaderMock;

  @PutMapping("/map/{mapPolicy}")
  public ResponseEntity switchMapPolicyTo(@PathVariable String mapPolicy) {
    val castedMapPolicy = MapPolicy.valueOf(mapPolicy);
    rateLoaderMock.setMapper(castedMapPolicy);
    return ResponseEntity.noContent().build();
  }

  @PutMapping("/reduce/{reducePolicy}")
  public ResponseEntity switchReducePolicyTo(@PathVariable String reducePolicy) {
    val castedReducePolicy = ReducePolicy.valueOf(reducePolicy);
    rateLoaderMock.setReducer(castedReducePolicy);
    return ResponseEntity.noContent().build();
  }
}
