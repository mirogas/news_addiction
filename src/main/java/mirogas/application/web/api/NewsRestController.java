package mirogas.application.web.api;

import java.time.LocalDate;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.service.news.NewsLoader;
import mirogas.application.service.news.NewsService;
import mirogas.application.web.mapper.NewsMapper;
import mirogas.application.web.model.NewsUI;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * news_addiction Created on 23.06.17.
 */

@RestController
@RequestMapping(value = "api/news")
@RequiredArgsConstructor
public class NewsRestController {

  private final NewsService newsService;
  private final NewsLoader newsLoader;
  private final NewsMapper newsMapper;

  @PostMapping("load")
  public List<NewsUI> load(
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    return newsMapper.map(newsService.addAll(
        newsLoader.loadByInterval(
            new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end))
        )));
  }

  @GetMapping
  public List<NewsUI> show(
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    return newsMapper.map(newsService.findNewsByInterval(
        new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end))
    ));
  }
}
