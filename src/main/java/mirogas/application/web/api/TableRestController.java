package mirogas.application.web.api;

import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import mirogas.application.model.object.Currency;
import mirogas.application.service.TableService;
import mirogas.application.service.report.ReportService;
import mirogas.application.web.model.TableResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/table")
@RequiredArgsConstructor
public class TableRestController {

  private final ReportService reportService;
  private final TableService tableService;

  @GetMapping("/{currency}")
  public ResponseEntity<TableResult> getTableFor(
      @PathVariable("currency") @NonNull String currency,
      @RequestParam @NonNull String[] reportNames
  ) {
    val castedCurrency = Currency.valueOf(currency);
    val reports = Arrays.stream(reportNames)
        .map(name -> reportService.findByNameAndCurrency(name, castedCurrency))
        .collect(Collectors.toList());

    return ResponseEntity.ok(tableService.estimate(reports, castedCurrency));
  }

}
