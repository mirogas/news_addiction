package mirogas.application.web.api;

import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.Report;
import mirogas.application.service.report.ReportService;
import mirogas.application.web.mapper.ReportMapper;
import mirogas.application.web.model.ReportUI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * news_addiction Created on 09.06.17.
 */

@RestController
@RequestMapping("api/report")
@RequiredArgsConstructor
public class ReportRestController {

  private final ReportService reportService;
  private final ReportMapper reportMapper;

  @GetMapping(value = "/name/{reportName}/currency/{currency}")
  public ReportUI getReport(
      @PathVariable(name = "reportName") @NonNull final String reportName,
      @PathVariable(name = "currency") @NonNull final String currency
  ) {
    final Report report = reportService
        .findByNameAndCurrency(reportName, Currency.valueOf(currency));

    return reportMapper.map(report);
  }

  @GetMapping
  public ResponseEntity<List<ReportUI>> findAll() {
    return ResponseEntity.ok(reportService.findAll()
        .stream()
        .map(reportMapper::map)
        .collect(Collectors.toList()));
  }

  @GetMapping("/possible")
  public ResponseEntity<List<String>> getAllPossibleReports() {
    return ResponseEntity.ok(reportService.findAllNames());
  }
}
