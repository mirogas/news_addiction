package mirogas.application.web.api;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import mirogas.analizing.Analyzer;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/analyzers")
@RequiredArgsConstructor
@Slf4j
public class AnalyzerController {

  private final List<Analyzer> analyzers;

  @GetMapping("/all")
  public ResponseEntity<List<String>> getAllAnalyzerNames() {
    return ResponseEntity.ok(analyzers.stream()
        .map(Analyzer::getClass)
        .map(Class::getSimpleName)
        .collect(Collectors.toList()));
  }

  @PutMapping("/all/analyze/currency/{currency}")
  public ResponseEntity analyze(
      @PathVariable("currency") @NonNull final String currency,
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    val samplingInterval = new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end));
    val castedCurrency = Currency.valueOf(currency);

    analyzers.forEach(analyzer -> analyzeExceptionally(analyzer, castedCurrency, samplingInterval));
    return ResponseEntity.noContent().build();
  }

  @PutMapping
  public ResponseEntity predicate(
      @RequestParam("start") @NonNull final String start,
      @RequestParam("end") @NonNull final String end
  ) {
    val samplingInterval = new SamplingInterval(LocalDate.parse(start), LocalDate.parse(end));

    analyzers.forEach(
        analyzer -> analyzer.predicate(samplingInterval)
    );

    return ResponseEntity.noContent().build();
  }

  private void analyzeExceptionally(
      Analyzer analyzer, Currency currency, SamplingInterval samplingInterval
  ) {
    try {
      log.info("{} start analyze", analyzer.getClass().getSimpleName());
      analyzer.analyze(samplingInterval, currency);
      log.info("{} end analyze", analyzer.getClass().getSimpleName());
    } catch (RuntimeException e) {
      log.error("{} can't analyze with exception {}", analyzer.getClass().getSimpleName(),
          e.getMessage());
    }
  }
}
