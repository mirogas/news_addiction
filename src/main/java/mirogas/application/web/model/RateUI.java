package mirogas.application.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * news_addiction Created on 09.06.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RateUI {

  private double rate;
  private String date;
}
