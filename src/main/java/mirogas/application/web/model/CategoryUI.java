package mirogas.application.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * news_addiction Created on 23.06.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryUI {

  private String category;
  private int repeats;
}
