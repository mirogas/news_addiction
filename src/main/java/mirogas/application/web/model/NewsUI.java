package mirogas.application.web.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * news_addiction Created on 23.06.17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewsUI {

  private String date;
  private List<CategoryUI> categories;
}
