package mirogas.application.model.object;

import java.time.LocalDate;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * news_addiction Created by igor on 22.04.17.
 */

@Getter
@Builder
@EqualsAndHashCode
public class PredicationRate {

  @Setter
  private long predicationRateId;
  private double rate;
  private LocalDate rateDate;
  private long reportId;
}
