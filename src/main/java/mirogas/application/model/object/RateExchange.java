package mirogas.application.model.object;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * news_addiction Created by igor on 08.04.17.
 */

@Data
@Builder
@EqualsAndHashCode
public class RateExchange implements Comparable<RateExchange> {

  private long rateId;
  private double rate;
  private Currency currency;
  private LocalDate date;

  @Override
  public int compareTo(final @NonNull RateExchange rateExchange) {
    return date.compareTo(rateExchange.getDate());
  }
}