package mirogas.application.model.object;

import java.util.List;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * news_addiction Created by igor on 22.04.17.
 */

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Report {

  private long reportId;
  private String name;
  private Currency currency;

  private List<PredicationRate> predicationRates;
}
