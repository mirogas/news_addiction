package mirogas.application.model.object;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * news_addiction Created by igor on 09.04.17.
 */

@Data
@Builder
@EqualsAndHashCode(exclude = "repeats")
public class Category {

  private long categoryId;
  private long newsId;
  private String categoryName;
  private int repeats;

  public static Category fromString(final String categoryName) {
    return Category.builder().categoryName(categoryName).build();
  }
}
