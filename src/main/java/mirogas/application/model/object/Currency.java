package mirogas.application.model.object;

/**
 * news_addiction Created by igor on 22.04.17.
 */
public enum Currency {
  RUB,
  EUR,
  USD,
  GBP;

  public static Currency getCurrency(final String currency) {
    if (RUB.name().equals(currency)) {
      return RUB;
    } else if (EUR.name().equals(currency)) {
      return EUR;
    } else if (USD.name().equals(currency)) {
      return USD;
    } else if (GBP.name().equals(currency)) {
      return GBP;
    } else {
      throw new RuntimeException(currency + " not supported");
    }
  }
}
