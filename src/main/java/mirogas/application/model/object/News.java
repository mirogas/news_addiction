package mirogas.application.model.object;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * news_addiction Created by igor on 08.04.17.
 */

@Data
@Builder
@EqualsAndHashCode
public class News implements Comparable<News> {

  private long newsId;
  private LocalDate newsDate;

  @Builder.Default
  private Map<Category, Integer> categories = Collections.emptyMap();

  public int compareTo(final @NonNull News news) {
    return newsDate.compareTo(news.getNewsDate());
  }
}
