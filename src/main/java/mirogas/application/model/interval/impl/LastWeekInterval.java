package mirogas.application.model.interval.impl;

import java.time.LocalDate;
import mirogas.application.model.interval.Interval;
import mirogas.application.model.interval.SamplingInterval;

/**
 * news_addiction Created on 09.06.17.
 */

public class LastWeekInterval implements Interval {

  public static SamplingInterval create() {
    return new LastWeekInterval().get();
  }

  @Override
  public SamplingInterval get() {
    final LocalDate now = LocalDate.now();

    final int dayOfWeek = now.getDayOfWeek().getValue();
    final LocalDate lastDayOfLastWeek = now.minusDays(dayOfWeek);

    return SamplingInterval.builder()
        .start(lastDayOfLastWeek.minusDays(6))
        .end(lastDayOfLastWeek)
        .build();
  }
}
