package mirogas.application.model.interval.impl;

import java.time.LocalDate;
import mirogas.application.model.interval.Interval;
import mirogas.application.model.interval.SamplingInterval;

/**
 * news_addiction Created on 09.06.17.
 */
public class LastMonthInterval implements Interval {

  public static SamplingInterval create() {
    return new LastWeekInterval().get();
  }

  @Override
  public SamplingInterval get() {
    final LocalDate lastDayOfLastMonth = LocalDate.now().withDayOfMonth(1).minusDays(1);

    return SamplingInterval.builder()
        .start(lastDayOfLastMonth.withDayOfMonth(1))
        .end(lastDayOfLastMonth)
        .build();
  }
}
