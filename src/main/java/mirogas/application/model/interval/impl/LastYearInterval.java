package mirogas.application.model.interval.impl;

import java.time.LocalDate;
import mirogas.application.model.interval.Interval;
import mirogas.application.model.interval.SamplingInterval;

/**
 * news_addiction Created on 09.06.17.
 */
public class LastYearInterval implements Interval {

  public static SamplingInterval create() {
    return new LastWeekInterval().get();
  }

  @Override
  public SamplingInterval get() {
    final LocalDate lastDayOfLastYear = LocalDate.now().withDayOfYear(1).minusDays(1);

    return SamplingInterval.builder()
        .start(lastDayOfLastYear.withDayOfYear(1))
        .end(lastDayOfLastYear)
        .build();

  }
}
