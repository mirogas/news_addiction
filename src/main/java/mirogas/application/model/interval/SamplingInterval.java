package mirogas.application.model.interval;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * news_addiction Created on 09.06.17.
 */

@AllArgsConstructor
@Getter
@Builder
@EqualsAndHashCode
public class SamplingInterval {

  private LocalDate start;
  private LocalDate end;
}
