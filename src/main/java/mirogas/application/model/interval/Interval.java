package mirogas.application.model.interval;

/**
 * news_addiction Created on 09.06.17.
 */

@FunctionalInterface
public interface Interval {

  SamplingInterval get();
}
