package mirogas.application;

/**
 * news_addiction Created by igor on 22.04.17.
 */
public interface EnvironmentConstant {

  String SPRING_PROFILE_TEST = "test";
  String SPRING_PROFILE_PROD = "prod";
  String SPRING_PROFILE_DEV = "dev";
  String SPRING_PROFILE_RANDOM_DATA = "random";
}
