package mirogas.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * news_addiction Created by igor on 08.04.17.
 */
@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
@ComponentScan(basePackages = "mirogas.*")
public class Application {

  public static void main(final String[] args) {
    SpringApplication.run(new Class<?>[]{
        Application.class
    }, args);
  }
}
