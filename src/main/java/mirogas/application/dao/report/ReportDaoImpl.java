package mirogas.application.dao.report;

import static org.springframework.dao.support.DataAccessUtils.singleResult;

import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.Report;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * news_addiction Created by igor on 22.04.17.
 */
@Repository
@AllArgsConstructor
public class ReportDaoImpl implements ReportDao {

  private static final RowMapper<Report> REPORT_ROW_MAPPER = (rs, rowNum) ->
      Report.builder()
          .currency(Currency.valueOf(rs.getString("currency")))
          .name(rs.getString("report_name"))
          .reportId(rs.getLong("report_id"))
          .build();

  private JdbcTemplate jdbcTemplate;

  @Override
  public List<Report> findAll() {
    return jdbcTemplate.query(
        "SELECT * FROM reports",
        REPORT_ROW_MAPPER
    );
  }

  @Override
  public Optional<Report> findByNameAndCurrency(final String name, final String currency) {
    return Optional.ofNullable(
        singleResult(
            jdbcTemplate.query(
                "SELECT * FROM reports WHERE report_name = ? and currency = ?",
                new Object[]{name, currency},
                REPORT_ROW_MAPPER
            )
        )
    );
  }

  @Override
  public Report add(final String name, final String currency) {
    return jdbcTemplate.queryForObject(
        "INSERT INTO reports(report_name, currency) VALUES (?, ?) RETURNING *",
        new Object[]{name, currency},
        REPORT_ROW_MAPPER
    );
  }
}
