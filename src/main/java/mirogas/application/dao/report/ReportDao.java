package mirogas.application.dao.report;

import java.util.List;
import java.util.Optional;
import mirogas.application.model.object.Report;

/**
 * news_addiction Created by igor on 22.04.17.
 */
public interface ReportDao {

  List<Report> findAll();

  Optional<Report> findByNameAndCurrency(String name, String currency);

  Report add(String name, String currency);
}
