package mirogas.application.dao.predication;

import java.time.LocalDate;
import java.util.List;
import mirogas.application.model.object.PredicationRate;

/**
 * news_addiction Created by igor on 22.04.17.
 */
public interface PredicationRateDao {

  List<PredicationRate> findAllByReportId(long reportId);

  PredicationRate add(long reportId, double rate, LocalDate date);

  List<PredicationRate> addAll(List<PredicationRate> predicationRates);
}
