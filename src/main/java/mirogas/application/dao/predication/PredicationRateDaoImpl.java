package mirogas.application.dao.predication;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import mirogas.application.model.object.PredicationRate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * news_addiction Created by igor on 22.04.17.
 */

@Repository
@AllArgsConstructor
public class PredicationRateDaoImpl implements PredicationRateDao {

  private static final RowMapper<PredicationRate> PREDICATION_RATE_ROW_MAPPER = (rs, rowNum) ->
      PredicationRate
          .builder()
          .predicationRateId(rs.getLong("predication_rate_id"))
          .rate(rs.getDouble("rate"))
          .rateDate(rs.getDate("rate_date").toLocalDate())
          .reportId(rs.getLong("report_id"))
          .build();

  private static final String ADD_SQL = "INSERT INTO prediction_rates(rate, rate_date, report_id)" +
      " VALUES (?, ?, ?) RETURNING *";
  private JdbcTemplate jdbcTemplate;

  @Override
  public List<PredicationRate> findAllByReportId(final long reportId) {
    return jdbcTemplate.query(
        "SELECT * FROM prediction_rates WHERE report_id = ?",
        new Object[]{reportId},
        PREDICATION_RATE_ROW_MAPPER
    );
  }

  @Override
  public PredicationRate add(final long reportId, final double rate, final LocalDate date) {
    return jdbcTemplate.queryForObject(
        ADD_SQL,
        new Object[]{rate, date, reportId},
        PREDICATION_RATE_ROW_MAPPER
    );
  }

  @Override
  public List<PredicationRate> addAll(List<PredicationRate> predicationRates) {
    final List<Object[]> args = predicationRates.stream()
        .map(
            pR -> new Object[]{pR.getRate(), pR.getRateDate(), pR.getReportId()}
        )
        .collect(Collectors.toList());

    final int[] ids = jdbcTemplate.batchUpdate(
        ADD_SQL, args
    );

    for (int i = 0; i < predicationRates.size(); i++) {
      predicationRates.get(i).setPredicationRateId(ids[i]);
    }

    return predicationRates;
  }
}
