package mirogas.application.dao.category;

import java.util.List;
import mirogas.application.model.object.Category;

/**
 * news_addiction Created by igor on 09.04.17.
 */
public interface CategoryDao {

  List<Category> findAllByNewsId(long newsId);

  Category add(long newsId, String categoryName, int repeats);

  List<Category> addAll(final List<Category> categories);
}
