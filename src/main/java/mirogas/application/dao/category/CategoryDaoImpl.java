package mirogas.application.dao.category;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import mirogas.application.model.object.Category;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * news_addiction Created by igor on 09.04.17.
 */

@Repository
@AllArgsConstructor
public class CategoryDaoImpl implements CategoryDao {

  private final static RowMapper<Category> CATEGORY_ROW_MAPPER =
      (resultSet, rowNumber) -> Category.builder()
          .categoryId(resultSet.getLong("category_id"))
          .newsId(resultSet.getLong("news_id"))
          .categoryName(resultSet.getString("category_name"))
          .repeats(resultSet.getInt("repeats"))
          .build();
  private static final String ADD_SQL = "INSERT INTO categories(news_id,category_name, repeats)" +
      " VALUES(?, ?, ?) RETURNING *";
  private final JdbcTemplate jdbcTemplate;

  @Override
  public List<Category> findAllByNewsId(final long newsId) {
    return jdbcTemplate.query(
        "SELECT * FROM categories WHERE news_id = ?",
        new Object[]{newsId},
        CATEGORY_ROW_MAPPER
    );
  }

  @Override
  public Category add(final long newsId, final String categoryName, final int repeats) {
    return jdbcTemplate.queryForObject(
        ADD_SQL,
        new Object[]{newsId, categoryName, repeats},
        CATEGORY_ROW_MAPPER
    );
  }

  @Override
  public List<Category> addAll(final List<Category> categories) {
    final List<Object[]> args = categories.stream()
        .map(
            category -> new Object[]
                {category.getNewsId(), category.getCategoryName(), category.getRepeats()}
        )
        .collect(Collectors.toList());
    final int[] categoryIds = jdbcTemplate.batchUpdate(
        ADD_SQL, args
    );

    for (int i = 0; i < categories.size(); i++) {
      categories.get(i).setCategoryId(categoryIds[i]);
    }

    return categories;
  }
}
