package mirogas.application.dao.news;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import mirogas.application.model.object.News;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * news_addiction Created by igor on 08.04.17.
 */

@Repository
@AllArgsConstructor
public class NewsDaoImpl implements NewsDao {

  private static final RowMapper<News> NEWS_ROW_MAPPER_NON_CATEGORIZED =
      (resultSet, rowNumber) -> News.builder()
          .newsId(resultSet.getLong("news_id"))
          .newsDate(resultSet.getDate("news_date").toLocalDate())
          .build();
  final private JdbcTemplate jdbcTemplate;

  @Override
  public List<News> findAll() {
    return jdbcTemplate.query(
        "SELECT * FROM news",
        NEWS_ROW_MAPPER_NON_CATEGORIZED
    );
  }

  @Override
  public List<News> findBetween(final LocalDate start, final LocalDate end) {
    return jdbcTemplate.query(
        "SELECT * FROM news WHERE news_date >= ? AND news_date <= ?",
        new Object[]{start, end},
        NEWS_ROW_MAPPER_NON_CATEGORIZED
    );
  }

  @Override
  public News add(final LocalDate date) {
    return jdbcTemplate.queryForObject(
        "INSERT INTO news(news_date) VALUES (?) RETURNING *",
        new Object[]{date},
        NEWS_ROW_MAPPER_NON_CATEGORIZED
    );
  }
}
