package mirogas.application.dao.news;

import java.time.LocalDate;
import java.util.List;
import mirogas.application.model.object.News;

/**
 * news_addiction Created by igor on 08.04.17.
 */
public interface NewsDao {

  List<News> findAll();

  List<News> findBetween(LocalDate start, LocalDate end);

  News add(LocalDate date);
}
