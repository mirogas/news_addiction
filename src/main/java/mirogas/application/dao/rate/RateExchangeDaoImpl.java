package mirogas.application.dao.rate;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * news_addiction Created by igor on 08.04.17.
 */

@Repository
@AllArgsConstructor
public class RateExchangeDaoImpl implements RateExchangeDao {

  private static final RowMapper<RateExchange> RATE_EXCHANGE_ROW_MAPPER =
      (resultSet, rowNumber) -> RateExchange.builder()
          .rateId(resultSet.getLong("rate_id"))
          .rate(resultSet.getDouble("rate"))
          .currency(Currency.getCurrency(resultSet.getString("currency")))
          .date(resultSet.getDate("rate_date").toLocalDate())
          .build();
  private static final String ADD_SQL = "INSERT INTO rates(rate, currency, rate_date) VALUES (?, ?, ?) RETURNING *";

  final private JdbcTemplate jdbcTemplate;

  @Override
  public List<RateExchange> findAll() {
    return jdbcTemplate.query("Select * From rates", RATE_EXCHANGE_ROW_MAPPER);
  }

  @Override
  public RateExchange add(final double rate, final String currency, final LocalDate date) {
    return jdbcTemplate.queryForObject(
        ADD_SQL,
        new Object[]{rate, currency, date},
        RATE_EXCHANGE_ROW_MAPPER
    );
  }

  @Override
  public List<RateExchange> addAll(final List<RateExchange> rateExchanges) {
    final List<Object[]> args = rateExchanges.stream()
        .map(
            rate -> new Object[]{rate.getRate(), rate.getCurrency().name(), rate.getDate()}
        )
        .collect(Collectors.toList());

    final int[] ids = jdbcTemplate.batchUpdate(ADD_SQL, args);

    for (int i = 0; i < rateExchanges.size(); i++) {
      rateExchanges.get(i).setRateId(ids[i]);
    }

    return rateExchanges;
  }

  @Override
  public List<RateExchange> findBetweenWithCurrency(final LocalDate start, final LocalDate end,
      final String currency) {
    return jdbcTemplate.query(
        "SELECT * FROM rates WHERE rate_date >= ? AND rate_date <= ? AND rates.currency = ?",
        new Object[]{start, end, currency},
        RATE_EXCHANGE_ROW_MAPPER
    );
  }
}
