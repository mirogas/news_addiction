package mirogas.application.dao.rate;

import java.time.LocalDate;
import java.util.List;
import mirogas.application.model.object.RateExchange;

/**
 * news_addiction Created by igor on 08.04.17.
 */
public interface RateExchangeDao {

  List<RateExchange> findAll();

  RateExchange add(double rate, String currency, LocalDate date);

  List<RateExchange> addAll(List<RateExchange> rateExchanges);

  List<RateExchange> findBetweenWithCurrency(LocalDate start, LocalDate end, String currency);
}
