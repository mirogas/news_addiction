package mirogas.application;

import java.time.format.DateTimeFormatter;

/**
 * news_addiction Created on 09.06.17.
 */
public interface DateFormatConstant {

  String DATE_FORMAT = "yyyy-MM-dd";
  DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
}
