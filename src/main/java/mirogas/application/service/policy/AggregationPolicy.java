package mirogas.application.service.policy;

public enum AggregationPolicy {
  ONLY_FIRST,
  SUM,
  MULTIPLE,
  SMALLEST
}
