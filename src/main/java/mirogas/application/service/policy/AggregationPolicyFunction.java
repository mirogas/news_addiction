package mirogas.application.service.policy;

@FunctionalInterface
public interface AggregationPolicyFunction {

  double apply(double[] x);
}
