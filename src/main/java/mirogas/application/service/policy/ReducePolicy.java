package mirogas.application.service.policy;

import java.util.Arrays;
import lombok.Getter;

@Getter
public enum ReducePolicy {
  ONLY_FIRST(doubleArr -> doubleArr[0]),
  SUM(doubleArr -> Arrays.stream(doubleArr).sum()),
  MULTIPLE(doubleArr -> Arrays.stream(doubleArr).reduce(0, (x1, x2) -> 0.01 * x1 * x2)),
  SMALLEST(doubleArr -> Arrays.stream(doubleArr).min().orElseThrow(RuntimeException::new)),
  BIGGEST(doubleArr -> Arrays.stream(doubleArr).max().orElseThrow(RuntimeException::new)),
  AVERAGE(doubleArr -> Arrays.stream(doubleArr).average().orElseThrow(RuntimeException::new));

  private Reducer reducer;

  ReducePolicy(Reducer reducer) {
    this.reducer = reducer;
  }
}
