package mirogas.application.service.policy;

@FunctionalInterface
public interface Reducer {

  double reduce(double[] x);
}
