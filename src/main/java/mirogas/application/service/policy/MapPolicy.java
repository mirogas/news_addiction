package mirogas.application.service.policy;

import lombok.Getter;

@Getter
public enum MapPolicy {
  LINEAR(x -> 7 * x + 5),
  QUADRATIC(x -> 0.1 * x * x - 2 * x + 7),
  SIN(Math::sin),
  LOGARITHMIC(Math::log),
  SQUARE(Math::sqrt);

  private Mapper mapper;

  MapPolicy(Mapper mapper) {
    this.mapper = mapper;
  }
}
