package mirogas.application.service.policy;

@FunctionalInterface
public interface Mapper {

  double map(double x);
}
