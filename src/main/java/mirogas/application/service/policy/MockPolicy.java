package mirogas.application.service.policy;

public enum MockPolicy {
  LINEAR,
  QUADRATIC,
  SIN,
  LOGARITHMIC,
  SQUARE
}
