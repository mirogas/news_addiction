package mirogas.application.service.policy;

import java.util.List;
import mirogas.application.model.object.News;

@FunctionalInterface
public interface MockPolicySupplier {

  List<News> supply();
}
