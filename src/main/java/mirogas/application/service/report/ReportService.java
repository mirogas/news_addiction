package mirogas.application.service.report;

import java.util.List;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.Report;

/**
 * news_addiction Created on 09.06.17.
 */
public interface ReportService {

  Report findByNameAndCurrency(String name, Currency currency);

  List<Report> findAll();

  Report add(Class clazz, Currency currency);

  List<String> findAllNames();
}
