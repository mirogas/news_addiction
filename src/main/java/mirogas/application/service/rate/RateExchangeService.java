package mirogas.application.service.rate;

import java.time.LocalDate;
import java.util.List;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;

/**
 * news_addiction Created on 10.06.17.
 */

public interface RateExchangeService {

  List<RateExchange> findBetweenWithCurrency(SamplingInterval samplingInterval, Currency currency);

  RateExchange add(double rate, Currency currency, LocalDate date);

  List<RateExchange> addAll(List<RateExchange> rateExchanges);
}
