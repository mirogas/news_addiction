package mirogas.application.service.rate;

import java.io.IOException;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.DateFormatConstant;
import mirogas.application.model.object.Currency;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 05.06.2017.
 */
@Service
@Slf4j
public class GetRateInDate {

  private static final String CONNECTION_URL = "http://www.cbr.ru/currency_base/daily.aspx?date_req=%s.%s.%s";

  public double selectRate(final LocalDate date, final Currency currency) {
    final String[] now = date.format(DateFormatConstant.DATE_TIME_FORMATTER).split("-");
    final String year = now[0];
    final String month = now[1];
    final String day = now[2];

    String rate = "0";

    try {
      final Document doc = Jsoup.connect(String.format(CONNECTION_URL, day, month, year)).get();

      if (currency == Currency.USD) {
        rate = doc.select("tr:nth-child(13) td:nth-child(5)").text(); //USD
      } else if (currency == Currency.EUR) {
        rate = doc.select("tr:nth-child(12) td:nth-child(5)").text(); //EUR
      }

      return Double.parseDouble(rate.replace(",", "."));
    } catch (final IOException e) {
      log.error("Can't load or parse page\nWith error {}", e.getMessage());
      return 0.0;
    }
  }
}
