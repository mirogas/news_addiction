package mirogas.application.service.rate;

import java.util.List;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;

/**
 * news_addiction Created on 09.06.17.
 */
public interface RateLoader {

  List<RateExchange> loadByInterval(SamplingInterval interval, Currency currency);
}
