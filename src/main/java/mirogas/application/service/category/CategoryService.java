package mirogas.application.service.category;

import java.util.List;
import java.util.Map;
import mirogas.application.model.object.Category;

/**
 * news_addiction Created on 23.06.17.
 */
public interface CategoryService {

  Map<Category, Integer> findAllByNewsId(long newsId);

  Category add(long newsId, String categoryName, int repeats);

  List<Category> addAll(final List<Category> categories);
}
