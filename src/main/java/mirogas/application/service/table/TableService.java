package mirogas.application.service.table;

import java.util.List;
import java.util.Map;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.Report;

public interface TableService {

  Map<String, Map<String, Double>> estimate(List<Report> reports, Currency currency);
}
