package mirogas.application.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import mirogas.application.model.object.Category;

/**
 * news_addiction Created on 23.06.17.
 */
public class MapperUtil {

  @SafeVarargs
  public static <T> Map<T, Integer> count(final T... ts) {
    final Map<T, Integer> result = new HashMap<>();
    for (final T t : ts) {
      putToMapWithCount(result, t);
    }

    return result;
  }

  public static <T> Map<T, Integer> count(final List<T> list) {
    final Map<T, Integer> result = new HashMap<>();
    for (final T t : list) {
      putToMapWithCount(result, t);
    }

    return result;
  }

  private static <T> void putToMapWithCount(final Map<T, Integer> map, final T key) {
    final Integer value = map.get(key);
    if (value == null) {
      map.put(key, 1);
    } else {
      map.put(key, value + 1);
    }
  }

  public static Map<Category, Integer> mapToCategories(final List<String> news) {
    final Map<Category, Integer> count = count(
        news.stream()
            .map(Category::fromString)
            .collect(Collectors.toList())
    );

    count.forEach(Category::setRepeats);
    return count;
  }
}
