package mirogas.application.service.impl;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import mirogas.analizing.result.ErrorEstimator;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.RateExchange;
import mirogas.application.model.object.Report;
import mirogas.application.service.TableService;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.web.model.TableResult;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TableServiceImpl<T> implements TableService {

  private final List<ErrorEstimator<Double>> errorEstimators;
  private final RateExchangeService rateExchangeService;

  @Override
  public TableResult estimate(List<Report> reports, Currency currency) {
    log.info("Started estimation for {} reports", reports.size());
    final List<LocalDate> collect = reports.stream()
        .flatMap(r -> r.getPredicationRates().stream())
        .map(PredicationRate::getRateDate)
        .sorted()
        .collect(Collectors.toList());

    val start = collect.get(0);
    val end = collect.get(collect.size() - 1);

    val realValues = rateExchangeService.findBetweenWithCurrency(
        new SamplingInterval(start, end), currency
    ).stream().map(RateExchange::getRate).collect(Collectors.toList());

    List<String> verticalNames = Stream.concat(
        Stream.of("reportName"),
        errorEstimators.stream().map(ErrorEstimator::getClass).map(Class::getSimpleName))
        .collect(Collectors.toList());

    List<String> horizontalNames = reports.stream()
        .map(Report::getName)
        .collect(Collectors.toList());

    List<List<Double>> values = new ArrayList<>();

    for (final Report report : reports) {
      final List<PredicationRate> predicationRates = report.getPredicationRates();

      val cur = predicationRates.get(0).getRateDate();
      final int between = (int) DAYS.between(start, cur);

      final List<Double> curValue = new ArrayList<>();
      values.add(curValue);
      for (ErrorEstimator<Double> errorEstimator : errorEstimators) {
        final List<Double> right = predicationRates.stream()
            .map(PredicationRate::getRate)
            .collect(Collectors.toList());

        final double estimate = errorEstimator
            .estimate(realValues.subList(between, realValues.size()), right);

        curValue.add(estimate);
      }
    }

    return TableResult.builder()
        .verticalNames(verticalNames)
        .horizontalNames(horizontalNames)
        .values(values)
        .build();
  }
}
