package mirogas.application.service.impl;

import static java.time.temporal.ChronoUnit.DAYS;
import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_RANDOM_DATA;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.val;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.news.NewsService;
import mirogas.application.service.policy.MapPolicy;
import mirogas.application.service.policy.Mapper;
import mirogas.application.service.policy.ReducePolicy;
import mirogas.application.service.policy.Reducer;
import mirogas.application.service.rate.RateLoader;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile(SPRING_PROFILE_RANDOM_DATA)
@RequiredArgsConstructor
@Primary
public class RateLoaderMock implements RateLoader {

  private final NewsService newsService;
  private Reducer reducer = ReducePolicy.ONLY_FIRST.getReducer();
  private Mapper mapper = MapPolicy.LINEAR.getMapper();

  @Override
  public List<RateExchange> loadByInterval(SamplingInterval interval, Currency currency) {
    val newsList = newsService.findNewsByInterval(interval);
    if (DAYS.between(interval.getStart(), interval.getEnd()) != newsList.size()) {
      throw new RuntimeException("You do not load news yet");
    }

    return newsList.stream()
        .map(news ->
            ImmutablePair.of(reducer.reduce(news.getCategories()
                    .values()
                    .stream()
                    .mapToDouble(Integer::doubleValue)
                    .map(mapper::map)
                    .toArray()
                ),
                news.getNewsDate()
            )
        )
        .map(rateAndDate -> RateExchange.builder()
            .date(rateAndDate.getRight())
            .currency(currency)
            .rate(rateAndDate.getLeft())
            .build()
        )
        .collect(Collectors.toList());
  }

  public void setMapper(MapPolicy mapPolicy) {
    mapper = mapPolicy.getMapper();
  }

  public void setReducer(ReducePolicy reducePolicy) {
    reducer = reducePolicy.getReducer();
  }
}
