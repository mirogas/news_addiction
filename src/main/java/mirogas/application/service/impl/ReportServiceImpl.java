package mirogas.application.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.dao.report.ReportDao;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.PredicationRate;
import mirogas.application.model.object.Report;
import mirogas.application.service.report.ReportService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 09.06.17.
 */

@Service
@AllArgsConstructor
@Slf4j
public class ReportServiceImpl implements ReportService {

  private final ReportDao reportDao;
  private final PredicationRateDao predicationRateDao;

  @Override
  public Report findByNameAndCurrency(final String name, final Currency currency) {
    final Optional<Report> reportOptional = reportDao.findByNameAndCurrency(name, currency.name());

    if (reportOptional.isPresent()) {
      final Report report = reportOptional.get();

      final List<PredicationRate> predicationRates = predicationRateDao
          .findAllByReportId(report.getReportId());
      report.setPredicationRates(predicationRates);

      return report;
    } else {
      log.error("Can't find report by name {} and currency {}", name, currency.name());
      throw new RuntimeException("Can't find report");
    }
  }

  @Override
  public List<Report> findAll() {
    return reportDao.findAll().stream()
        .peek(r -> r.setPredicationRates(predicationRateDao.findAllByReportId(r.getReportId())))
        .collect(Collectors.toList());
  }

  @Override
  public Report add(final Class clazz, final Currency currency) {
    return reportDao.add(clazz.getSimpleName(), currency.name());
  }

  @Override
  public List<String> findAllNames() {
    return reportDao.findAll().stream()
        .map(Report::getName)
        .collect(Collectors.toList());
  }
}
