package mirogas.application.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import mirogas.application.dao.rate.RateExchangeDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.rate.RateExchangeService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 10.06.17.
 */

@Service
@AllArgsConstructor
public class RateExchangeServiceImpl implements RateExchangeService {

  private final RateExchangeDao rateExchangeDao;

  @Override
  public List<RateExchange> findBetweenWithCurrency(
      final SamplingInterval samplingInterval, final Currency currency
  ) {
    return rateExchangeDao.findBetweenWithCurrency(
        samplingInterval.getStart(), samplingInterval.getEnd(), currency.name()
    ).stream()
        .sorted()
        .collect(Collectors.toList());
  }

  @Override
  public RateExchange add(final double rate, final Currency currency, final LocalDate date) {
    return rateExchangeDao.add(rate, currency.name(), date);
  }

  @Override
  public List<RateExchange> addAll(final List<RateExchange> rateExchanges) {
    return rateExchanges.stream()
        .map(rateExchange -> add(rateExchange.getRate(), rateExchange.getCurrency(),
            rateExchange.getDate()))
        .collect(Collectors.toList());
  }
}
