package mirogas.application.service.impl;

import static java.time.temporal.ChronoUnit.DAYS;
import static mirogas.application.EnvironmentConstant.SPRING_PROFILE_RANDOM_DATA;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.service.news.NewsLoader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile(SPRING_PROFILE_RANDOM_DATA)
@RequiredArgsConstructor
@Primary
public class NewsLoaderMock implements NewsLoader {

  private List<String> categoriesFromFile;

  @PostConstruct
  @SneakyThrows
  public void loadNewsFile() {
    categoriesFromFile = IOUtils.readLines(getClass().getResourceAsStream("/news_data"), "UTF-8");
  }

  @Override
  public List<News> loadByInterval(SamplingInterval interval) {
    final long daysAmount = DAYS.between(interval.getStart(), interval.getEnd());

    return Stream.iterate(interval.getStart(), c -> c.plusDays(1))
        .limit(daysAmount)
        .map(date -> News.builder()
            .categories(createCategories())
            .newsDate(date)
            .build()
        )
        .collect(Collectors.toList());
  }

  private Map<Category, Integer> createCategories() {
    return IntStream.range(0, RandomUtils.nextInt(5, 15))
        .map(i -> RandomUtils.nextInt(0, categoriesFromFile.size()))
        .mapToObj(categoriesFromFile::get)
        .map(category -> Category.builder().categoryName(category)
            .repeats(RandomUtils.nextInt(1, 20)).build())
        .collect(Collectors.toMap(
            Function.identity(), Category::getRepeats, (v1, v2) -> v1
        ));
  }
}
