package mirogas.application.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.dao.category.CategoryDao;
import mirogas.application.model.object.Category;
import mirogas.application.service.category.CategoryService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 23.06.17.
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

  private final CategoryDao categoryDao;

  @Override
  public Map<Category, Integer> findAllByNewsId(final long newsId) {
    final List<Category> allByNewsId = categoryDao.findAllByNewsId(newsId);

    return allByNewsId.stream()
        .collect(Collectors.toMap(Function.identity(), Category::getRepeats));
  }

  @Override
  public Category add(final long newsId, final String categoryName, final int repeats) {
    log.info("Add category with newsId = {}, categoryName = {}, repeats = {}", newsId, categoryName,
        repeats);
    return categoryDao.add(newsId, categoryName, repeats);
  }

  @Override
  public List<Category> addAll(List<Category> categories) {
    log.info("Add list of categories with size = {}", categories.size());
    return categoryDao.addAll(categories);
  }

}
