package mirogas.application.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import mirogas.application.dao.news.NewsDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.service.category.CategoryService;
import mirogas.application.service.news.NewsService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 10.06.17.
 */

@Service
@AllArgsConstructor
public class NewsServiceImpl implements NewsService {

  private final NewsDao newsDao;
  private final CategoryService categoryService;

  @Override
  public List<News> findNewsByInterval(final SamplingInterval samplingInterval) {
    final List<News> newsList = newsDao
        .findBetween(samplingInterval.getStart(), samplingInterval.getEnd())
        .stream()
        .sorted()
        .collect(Collectors.toList());

    newsList.forEach(
        news -> news.setCategories(
            categoryService.findAllByNewsId(news.getNewsId())
        )
    );

    return newsList;
  }

  @Override
  public List<News> addAll(final List<News> newsList) {
    return newsList.stream()
        .map(this::addNews)
        .collect(Collectors.toList());
  }

  private News addNews(News news) {
    final long newsId = newsDao.add(news.getNewsDate()).getNewsId();

    final Map<Category, Integer> categories = news.getCategories()
        .entrySet()
        .stream()
        .map(entry -> categoryService
            .add(newsId, entry.getKey().getCategoryName(), entry.getValue()))
        .collect(Collectors.toMap(
            Function.identity(),
            Category::getRepeats
        ));

    return News.builder()
        .newsId(newsId)
        .categories(categories)
        .newsDate(news.getNewsDate())
        .build();
  }
}
