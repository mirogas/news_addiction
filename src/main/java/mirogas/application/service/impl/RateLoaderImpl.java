package mirogas.application.service.impl;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.rate.GetRateInDate;
import mirogas.application.service.rate.RateLoader;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 09.06.17.
 */

@Service
@AllArgsConstructor
@Slf4j
public class RateLoaderImpl implements RateLoader {

  private final GetRateInDate getRateInDate;

  @Override
  public List<RateExchange> loadByInterval(final SamplingInterval interval,
      final Currency currency) {
    LocalDate startDate = interval.getStart();
    final LocalDate endDate = interval.getEnd();
    final List<RateExchange> arrayListRate = new ArrayList<>();
    final RateExchange.RateExchangeBuilder rateExchangeBuilder = RateExchange.builder()
        .currency(currency);

    final int days = (int) DAYS.between(startDate, endDate) + 1;
    final List<CompletableFuture<RateExchange>> futureRates = IntStream.range(0, days)
        .mapToObj(startDate::plusDays)
        .map(
            day -> CompletableFuture.supplyAsync(() -> getRateInDate.selectRate(day, currency))
                .thenApply(rate -> rateExchangeBuilder.date(day).rate(rate).build())
        )
        .collect(Collectors.toList());

    try {
      return CompletableFuture.allOf(futureRates.toArray(new CompletableFuture[0]))
          .thenApply(ignore -> futureRates)
          .get()
          .stream()
          .map(this::getExceptionally)
          .collect(Collectors.toList());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Interrupt\nWith error {}", e.getMessage());
    }

    return Collections.emptyList();
  }

  @SneakyThrows
  private RateExchange getExceptionally(CompletableFuture<RateExchange> future) {
    return future.get();
  }
}
