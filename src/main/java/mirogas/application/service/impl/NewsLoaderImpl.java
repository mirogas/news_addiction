package mirogas.application.service.impl;

import static java.time.temporal.ChronoUnit.DAYS;
import static mirogas.application.service.MapperUtil.mapToCategories;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.News;
import mirogas.application.service.news.GetNewsInDate;
import mirogas.application.service.news.NewsLoader;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 09.06.17.
 */

@Service
@AllArgsConstructor
@Slf4j
public class NewsLoaderImpl implements NewsLoader {

  private final GetNewsInDate getNewsInDate;

  @Override
  public List<News> loadByInterval(final SamplingInterval interval) {
    final LocalDate startDate = interval.getStart();
    final LocalDate endDate = interval.getEnd();

    final int days = (int) DAYS.between(startDate, endDate) + 1;
    final List<CompletableFuture<News>> futureNews = IntStream.range(0, days)
        .mapToObj(startDate::plusDays)
        .map(currentDate ->
            CompletableFuture
                .supplyAsync(() -> mapToCategories(getNewsInDate.selectNews(currentDate)))
                .thenApply(categories -> News.builder().newsDate(currentDate).categories(categories)
                    .build())
        )
        .collect(Collectors.toList());

    try {
      return CompletableFuture.allOf(futureNews.toArray(new CompletableFuture[0]))
          .thenApply(ignore -> futureNews)
          .get()
          .stream()
          .map(this::getExceptionally)
          .collect(Collectors.toList());
    } catch (InterruptedException | ExecutionException e) {
      log.error("Interrupt\nWith error {}", e.getMessage());
    }

    return Collections.emptyList();
  }

  @SneakyThrows
  private News getExceptionally(CompletableFuture<News> future) {
    return future.get();
  }
}
