package mirogas.application.service;

import java.util.List;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.Report;
import mirogas.application.web.model.TableResult;

public interface TableService {

  TableResult estimate(List<Report> reports, Currency currency);
}
