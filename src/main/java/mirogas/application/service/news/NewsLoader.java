package mirogas.application.service.news;

import java.util.List;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.News;

/**
 * news_addiction Created on 09.06.17.
 */
public interface NewsLoader {

  List<News> loadByInterval(SamplingInterval interval);
}
