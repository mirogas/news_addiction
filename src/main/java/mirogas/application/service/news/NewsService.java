package mirogas.application.service.news;

import java.util.List;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.News;

/**
 * news_addiction Created on 10.06.17.
 */
public interface NewsService {

  List<News> findNewsByInterval(SamplingInterval samplingInterval);

  List<News> addAll(List<News> newsList);
}
