package mirogas.application.service.news;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mirogas.application.DateFormatConstant;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 15.03.17.
 */
@Service
@Slf4j
public class GetNewsInDate {

  private static final String CONNECTION_URL = "http://www.fontanka.ru/fontanka/%s/%s/%s/all.html";

  public List<String> selectNews(final LocalDate date) {
    final String[] now = date.format(DateFormatConstant.DATE_TIME_FORMATTER).split("-");
    final String year = now[0];
    final String month = now[1];
    final String day = now[2];

    try {
      final Document doc = Jsoup.connect(String.format(CONNECTION_URL, year, month, day)).get();
      final List<Element> categories = new ArrayList<>(doc.select(".categorylink"));

      return categories.stream().map(Element::text).collect(Collectors.toList());
    } catch (final IOException e) {
      log.error("can't parse the page\nWith error {}", e.getMessage());
      return new ArrayList<>();
    }
  }
}
