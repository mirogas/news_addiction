package mirogas.application.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * news_addiction Created by igor on 22.04.17.
 */

@Configuration
public class HttpClientConfig {

  @Bean
  public HttpClient httpClient()
      throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    return HttpClients.custom()
        .setSSLContext(
            new SSLContextBuilder().loadTrustMaterial(null, (TrustStrategy) (arg0, arg1) -> true)
                .build())
        .setSSLHostnameVerifier(new NoopHostnameVerifier())
        .build();
  }
}
