package mirogas.analizing;

import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;

/**
 * news_addiction Created on 10.06.17.
 */
public interface Analyzer {

  double THRESHOLD = 1000.0;

  void analyze(SamplingInterval samplingInterval, Currency currency);

  long predicate(SamplingInterval samplingInterval);
}
