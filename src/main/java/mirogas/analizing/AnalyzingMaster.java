package mirogas.analizing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import mirogas.analizing.model.AnalyzingObject;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.News;
import mirogas.application.model.object.RateExchange;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 09.06.17.
 */

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
class AnalyzingMaster {

  private List<Category> currentCategories;
  private List<String> currentArgumentNames;

  static List<Double> fillArguments(final List<Category> allCategories,
      final Map<Category, Integer> categories) {
    return allCategories.stream()
        .map(category -> categories.getOrDefault(category, 0))
        .map(Integer::doubleValue)
        .collect(Collectors.toList());
  }

  static boolean checkListsForEquivalentDates(final List<RateExchange> rateExchanges,
      final List<News> newsList) {
    if (rateExchanges.size() != newsList.size()) {
      return false;
    }

    for (int i = 0; i < rateExchanges.size(); ++i) {
      if (!rateExchanges.get(i).getDate().isEqual(newsList.get(i).getNewsDate())) {
        return false;
      }
    }

    return true;
  }

  public List<AnalyzingObject> combine(final List<RateExchange> rateExchanges,
      final List<News> newsList) {
    if (!checkListsForEquivalentDates(rateExchanges, newsList)) {
      log.error("Can't combine because of different size of dates");
      throw new IllegalArgumentException("Can't combine because of different size of dates");
    }

    currentCategories = new ArrayList<>(newsList.stream()
        .flatMap(news -> news.getCategories().keySet().stream())
        .collect(Collectors.toSet()));

    currentArgumentNames = currentCategories.stream()
        .map(Category::getCategoryName)
        .collect(Collectors.toList());

    return IntStream.range(0, rateExchanges.size())
        .mapToObj(
            i -> AnalyzingObject.builder()
                .value(rateExchanges.get(i).getRate())
                .arguments(fillArguments(currentCategories, newsList.get(i).getCategories()))
                .build()
        )
        .collect(Collectors.toList());
  }

  public List<Category> getAllCategories() {
    return currentCategories;
  }

  public List<String> getArgumentsNames() {
    return currentArgumentNames;
  }
}
