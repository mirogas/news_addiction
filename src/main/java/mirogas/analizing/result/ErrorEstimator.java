package mirogas.analizing.result;

import java.util.List;

public interface ErrorEstimator<T extends Number> {

  double estimate(List<T> left, List<T> right);
}
