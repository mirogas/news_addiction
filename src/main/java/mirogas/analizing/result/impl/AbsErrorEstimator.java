package mirogas.analizing.result.impl;

import java.util.List;
import lombok.val;
import mirogas.analizing.result.ErrorEstimator;

public class AbsErrorEstimator implements ErrorEstimator<Double> {

  @Override
  public double estimate(List<Double> left, List<Double> right) {
    int minSize = Math.min(left.size(), right.size());

    double res = 0.0;

    for (int i = 0; i < minSize; i++) {
      val diff = Math.abs(left.get(i) - right.get(i));
      res += diff;
    }

    return res;

  }
}
