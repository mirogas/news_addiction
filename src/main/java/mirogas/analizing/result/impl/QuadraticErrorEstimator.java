package mirogas.analizing.result.impl;

import java.util.List;
import lombok.val;
import mirogas.analizing.result.ErrorEstimator;
import org.springframework.stereotype.Service;

@Service
public class QuadraticErrorEstimator implements ErrorEstimator<Double> {

  @Override
  public double estimate(List<Double> left, List<Double> right) {
    int minSize = Math.min(left.size(), right.size());

    double res = 0.0;

    for (int i = 0; i < minSize; i++) {
      val diff = left.get(i) - right.get(i);
      res += diff * diff;
    }

    return res;
  }
}
