package mirogas.analizing.result.impl;

import java.util.List;
import java.util.stream.IntStream;
import mirogas.analizing.result.ErrorEstimator;

public class MinDiffErrorEstimator implements ErrorEstimator<Double> {

  @Override
  public double estimate(List<Double> left, List<Double> right) {
    int minSize = Math.min(left.size(), right.size());

    return IntStream
        .range(0, minSize)
        .mapToDouble(i -> left.get(i) - right.get(i))
        .map(Math::abs)
        .min()
        .orElseThrow(RuntimeException::new);
  }
}
