package mirogas.analizing.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * news_addiction Created on 09.06.17.
 */

@Getter
@Builder
@AllArgsConstructor
public class AnalyzingObject {

  private double value;
  private List<Double> arguments;
}
