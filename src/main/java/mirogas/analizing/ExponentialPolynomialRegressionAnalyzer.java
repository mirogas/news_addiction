package mirogas.analizing;

import flanagan.analysis.RegressionFunction;
import mirogas.analizing.flanagan.function.ExponentialPolynomialFunction;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.service.news.NewsService;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.service.report.ReportService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 11.06.17.
 */

@Service
public class ExponentialPolynomialRegressionAnalyzer extends EasyPolynomialRegressionAnalyzer {

  public ExponentialPolynomialRegressionAnalyzer(
      final RateExchangeService rateExchangeService,
      final NewsService newsService,
      final ReportService reportService,
      final PredicationRateDao predicationRateDao,
      final AnalyzingMaster analyzingMaster
  ) {
    super(rateExchangeService, newsService, reportService, predicationRateDao, analyzingMaster);
  }

  @Override
  public RegressionFunction getFunction() {
    return new ExponentialPolynomialFunction();
  }
}
