package mirogas.analizing;

import flanagan.analysis.Regression;
import flanagan.analysis.RegressionFunction;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import mirogas.analizing.flanagan.function.EasyPolynomialFunction;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import mirogas.application.model.object.Report;
import mirogas.application.service.news.NewsService;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.service.report.ReportService;
import org.springframework.stereotype.Service;

/**
 * news_addiction Created on 10.06.17.
 */

@Service
public class EasyPolynomialRegressionAnalyzer extends BaseAnalyzer {

  private final Random random = new Random();
  private double[] evaluatedCoefficients;
  private RegressionFunction regressionFunction;

  public EasyPolynomialRegressionAnalyzer(
      final RateExchangeService rateExchangeService,
      final NewsService newsService,
      final ReportService reportService,
      final PredicationRateDao predicationRateDao,
      final AnalyzingMaster analyzingMaster
  ) {
    super(rateExchangeService, newsService, reportService, predicationRateDao, analyzingMaster);
  }

  @Override
  public void analyze(final SamplingInterval samplingInterval, final Currency currency) {
    combineToAnalyzingObject(samplingInterval, currency);

    final double[][] x = evalTransponseX();
    final double[] y = evalY();

    final double[] start = Collections.nCopies(x.length + 1, 0.5)
        .stream()
        .mapToDouble(Double::doubleValue)
        .toArray();
    final double[] step = new double[start.length];

    for (int i = 0; i < step.length; ++i) {
      step[i] = /*(random.nextDouble()/2 + 0.5)/100.0*/0.1;
    }

    regressionFunction = getFunction();

    final Regression regression = new Regression(x, y);

    regression.simplex(regressionFunction, start, step);
    evaluatedCoefficients = regression.getCoeff();
  }

  @Override
  public long predicate(final SamplingInterval samplingInterval) {
    final Report report = reportService.add(this.getClass(), currency);
    final List<News> newsList = newsService.findNewsByInterval(samplingInterval);

    for (final News news : newsList) {
      final double[] newX = AnalyzingMaster.fillArguments(allCategories, news.getCategories())
          .stream()
          .mapToDouble(o -> o)
          .toArray();

      final double newRate = regressionFunction.function(evaluatedCoefficients, newX);

      if (newRate > THRESHOLD) {
        predicationRateDao.add(report.getReportId(), THRESHOLD, news.getNewsDate());
      } else {
        predicationRateDao.add(report.getReportId(), newRate, news.getNewsDate());
      }

    }

    return report.getReportId();
  }

  public RegressionFunction getFunction() {
    return new EasyPolynomialFunction();
  }
}
