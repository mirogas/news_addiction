package mirogas.analizing;

import java.util.List;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import mirogas.application.model.object.Report;
import mirogas.application.service.news.NewsService;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.service.report.ReportService;
import org.springframework.stereotype.Service;
import smile.regression.LASSO;

/**
 * news_addiction Created on 09.06.17.
 */
@Service
public class EasyRegressionAnalyzer extends BaseAnalyzer {

  private LASSO lasso;

  public EasyRegressionAnalyzer(
      final RateExchangeService rateExchangeService,
      final NewsService newsService,
      final ReportService reportService,
      final PredicationRateDao predicationRateDao,
      final AnalyzingMaster analyzingMaster
  ) {
    super(rateExchangeService, newsService, reportService, predicationRateDao, analyzingMaster);
  }

  @Override
  public void analyze(final SamplingInterval samplingInterval, final Currency currency) {
    combineToAnalyzingObject(samplingInterval, currency);

    final double[] y = evalY();
    final double[][] x = evalX();

    lasso = new LASSO(x, y, 0.5);
  }

  @Override
  public long predicate(final SamplingInterval samplingInterval) {
    final Report report = reportService.add(this.getClass(), currency);
    final List<News> newsList = newsService.findNewsByInterval(samplingInterval);

    for (final News news : newsList) {
      final double[] newX = AnalyzingMaster.fillArguments(allCategories, news.getCategories())
          .stream()
          .mapToDouble(o -> o)
          .toArray();

      final double newRate = lasso.predict(newX);

      if (newRate > THRESHOLD) {
        predicationRateDao.add(report.getReportId(), newRate, news.getNewsDate());
      } else {
        predicationRateDao.add(report.getReportId(), THRESHOLD, news.getNewsDate());
      }
    }

    return report.getReportId();
  }
}
