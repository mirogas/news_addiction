package mirogas.analizing.flanagan.function;

import flanagan.analysis.RegressionFunction;

/**
 * news_addiction Created on 11.06.17.
 */
public class EasyPolynomialFunction implements RegressionFunction {

  public double function(final double[] p, final double[] x) {
    double sum = p[0];
    for (int i = 0; i < x.length - 1; ++i) {
      sum += x[i] * p[i + 1];
    }
    return sum;
  }
}
