package mirogas.analizing.flanagan.function;

import flanagan.analysis.RegressionFunction;

/**
 * news_addiction Created on 11.06.17.
 */
public class ToOneArgumentPolynomialFunction implements RegressionFunction {

  public double function(final double[] p, final double[] x) {
    double argument = 0;
    double multiplier = 1;
    for (final double aX : x) {
      argument += aX * multiplier;
      multiplier *= 10;
    }
    double sum = 0.0;
    double curM = 1.0;
    for (final double aP : p) {
      sum += curM * aP;
      curM *= argument;
    }

    return sum;
  }
}
