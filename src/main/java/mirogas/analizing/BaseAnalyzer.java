package mirogas.analizing;

import java.util.List;
import lombok.RequiredArgsConstructor;
import mirogas.analizing.model.AnalyzingObject;
import mirogas.application.dao.predication.PredicationRateDao;
import mirogas.application.model.interval.SamplingInterval;
import mirogas.application.model.object.Category;
import mirogas.application.model.object.Currency;
import mirogas.application.model.object.News;
import mirogas.application.model.object.RateExchange;
import mirogas.application.service.news.NewsService;
import mirogas.application.service.rate.RateExchangeService;
import mirogas.application.service.report.ReportService;
import org.apache.commons.lang3.ArrayUtils;

/**
 * news_addiction Created on 10.06.17.
 */

@RequiredArgsConstructor
public abstract class BaseAnalyzer implements Analyzer {

  protected final RateExchangeService rateExchangeService;
  protected final NewsService newsService;
  protected final ReportService reportService;
  protected final PredicationRateDao predicationRateDao;
  private final AnalyzingMaster analyzingMaster;

  protected List<Category> allCategories;
  protected Currency currency;
  private List<AnalyzingObject> analyzingObjects;

  protected void combineToAnalyzingObject(final SamplingInterval samplingInterval,
      final Currency currency) {
    this.currency = currency;
    final List<News> newsList = newsService.findNewsByInterval(samplingInterval);
    final List<RateExchange> rateExchanges = rateExchangeService
        .findBetweenWithCurrency(samplingInterval, currency);

    analyzingObjects = analyzingMaster.combine(rateExchanges, newsList);
    allCategories = analyzingMaster.getAllCategories();
  }

  protected double[][] evalX() {
    return analyzingObjects
        .stream()
        .map(AnalyzingObject::getArguments)
        .map(arguments -> arguments.toArray(new Double[arguments.size()]))
        .map(ArrayUtils::toPrimitive)
        .toArray(double[][]::new);
  }

  protected double[] evalY() {
    return analyzingObjects
        .stream()
        .mapToDouble(AnalyzingObject::getValue)
        .toArray();
  }

  protected double[][] evalTransponseX() {
    final double[][] result = new double[analyzingObjects.get(0).getArguments().size()][];

    for (int i = 0; i < analyzingObjects.get(0).getArguments().size(); ++i) {
      result[i] = new double[analyzingObjects.size()];
      for (int j = 0; j < analyzingObjects.size(); ++j) {
        result[i][j] = analyzingObjects.get(j).getArguments().get(i);
      }
    }

    return result;
  }
}
