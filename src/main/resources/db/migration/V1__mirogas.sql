CREATE TABLE rates (
  rate_id   SERIAL PRIMARY KEY,
  rate      NUMERIC    NOT NULL,
  currency  VARCHAR(3) NOT NULL,
  rate_date DATE       NOT NULL,
  UNIQUE (currency, rate_date)
);

CREATE TABLE news (
  news_id   SERIAL PRIMARY KEY,
  news_date DATE NOT NULL,
  UNIQUE (news_date)
);

CREATE TABLE categories (
  category_id   SERIAL PRIMARY KEY,
  news_id       INT REFERENCES news (news_Id) ON DELETE CASCADE,
  category_name VARCHAR(100),
  repeats       INT,
  UNIQUE (category_name, news_id)
);

CREATE TABLE reports (
  report_id SERIAL PRIMARY KEY,
  report_name      VARCHAR(50) NOT NULL,
  currency  VARCHAR(3)  NOT NULL,
  UNIQUE (report_name, currency)
);

CREATE TABLE prediction_rates (
  predication_rate_id SERIAL PRIMARY KEY,
  rate                NUMERIC NOT NULL,
  rate_date           DATE    NOT NULL,
  report_id           INT REFERENCES reports (report_id) ON DELETE CASCADE
);